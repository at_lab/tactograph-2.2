import Image
import ImageFilter
from scipy import *
from scipy.special import *

def check_pixel(x,y):
    global rows
    global cols
    
    if (x > rows-1) or (y > cols-1) or (x < 0) or (y < 0):
        return 0
    else:
        return 1

def check_point(last_point,present_point,temp_point,too_last_point):
    global rows
    global cols
    
    level0 = check_pixel(temp_point[0],temp_point[1])
    if level0 == 1:
        if temp_point != last_point:
            if temp_point != present_point:
                if temp_point != too_last_point:
                    return 1    
                
                else:
                    return 0

def stop(im):
    if im.mode == 'L':
        rows = im.size[0]
        columns = im.size[1]
        for i in range(0,rows):
            for j in range(0,columns):
                if (i == rows-1) and (j == columns-1):
                    print 'Total image scanned'
                    return None
                
                elif im.getpixel((i,j)) == 0:
                    return [i,j]
    else:
        print 'Expected grayscale image.Try the bilevel function'
        

def namer_txt(fname):
    name = ''
    for i in fname:
        if i == '.':
            break
        else:
            name += i
    name = name + '_path_coords.txt'
    return name



def bilevel(im,thres = 200):
    
    
    imtemp = im.convert('L')
    print 'Creating Bilevel Object for follow optimization'
    rows = im.size[0]
    columns = im.size[1]

    for i in range(0,rows):
        for j in range(0,columns):
            if imtemp.getpixel((i,j)) < thres:
                imtemp.putpixel((i,j),0)
            else:
                imtemp.putpixel((i,j),255)

    return imtemp

def edge_thick(im,box_size = 3):
    limit = int(box_size/2)

    
    im = im.filter(ImageFilter.CONTOUR)
    
    mat_kit = []
    
    for i in range(0,limit+1):
        mat_kit.append(i)
        mat_kit.append((0-i))
    

    rows = im.size[0]
    columns = im.size[1]

    path_coords = []

    for i in range(0,rows):
        for j in range(0,columns):
            if im.getpixel((i,j)) == 0:
                path_coords.append([i,j])

    for point in path_coords:
        for i in mat_kit:
            for j in mat_kit:
                tempx = point[0] + i
                tempy = point[1] + j
                flag = check_pixel(tempx,tempy)
                if flag == 0:
                    pass
                else:
                    im.putpixel((tempx,tempy),0)

    return im

def clean_t(im):

    rows = im.size[0]
    cols = im.size[1]

    

    tkit1 = [[0,0],[1,0],[1,-1],[1,1]]
    tkit2 = [[0,0],[-1,0],[-1,-1],[-1,1]]

    for i in range(rows):
        for j in range(cols):
            cntxr = 0
            cntxl = 0
            cntyr = 0
            cntyl = 0

            for h in tkit1:
                p = i+h[0]
                q = j+h[1]

                test = check_pixel(p,q)
                if test == 1:
                    if im.getpixel((p,q)) == 0:
                        cntyr += 1

            for h in tkit2:
                p = i+h[0]
                q = j+h[1]

                test = check_pixel(p,q)
                if test == 1:
                    if im.getpixel((p,q)) == 0:
                        cntyl += 1

            for h in tkit1:
                p = i+h[1]
                q = j+h[0]

                test = check_pixel(p,q)
                if test == 1:
                    if im.getpixel((p,q)) == 0:
                        cntxr += 1

            for h in tkit2:
                p = i+h[1]
                q = j+h[0]

                test = check_pixel(p,q)
                if test == 1:
                    if im.getpixel((p,q)) == 0:
                        cntxl += 1
            
            if ((cntyr == 4) or (cntyl== 4) or (cntxr == 4) or (cntxl == 4)):
                im.putpixel((i,j),255)

    return im
            
def clean_2x2(im):
    rows = im.size[0]
    cols = im.size[1]

    sqrkit = [0,1]


    for i in range(rows):
        for j in range(cols):
            cnt = 0
            for a in sqrkit:
                for b in sqrkit:
                    p = a+i
                    q = b+j
                    test = check_pixel(p,q)
                    if test == 1:
                        if im.getpixel((p,q)) == 0:
                            cnt += 1
            flaglt = 0
            flagrt = 0
            flaglb = 0
            flagrb = 0
            if cnt == 4:
                
                left_app_coords = [[i,j-1],[i-1,j-1],[i-1,j],[i+2,j+1],[i+2,j+2],[i+1,j+2]]
                
                right_app_coords = [[i+1,j-1],[i+2,j-1],[i+2,j],[i-1,j+1],[i-1,j+2],[i,j+2]]
                                
                for point in left_app_coords:
                    x = point[0]
                    y = point[1]
                    
                    test = check_pixel(x,y)
                    if test == 1:
                        
                        if im.getpixel((x,y)) == 0:
                            flag = 1                            

                for point in right_app_coords:
                    x = point[0]
                    y = point[1]

                    test = check_pixel(x,y)
                    if test == 1:
                        if im.getpixel((x,y)) == 0:
                            flag = 2
                            
                if (flag == 1):
                    
                    im.putpixel((i,j+1),255)
                    im.putpixel((i+1,j),255)
                if (flag == 2):
                    
                    im.putpixel((i,j),255)
                    im.putpixel((i+1,j+1),255)
            
    return im



def unisolate(im):
    global rows
    global cols
    black_coords = [];
    for i in range(rows):
        for j in range(cols):
            if im.getpixel((i,j)) == 0:
                black_coords.append([i,j])

    for point in black_coords:
        i = point[0]
        j = point[1]
        cnt = 0
        scan_kit = [-1,0,1]
        for a in scan_kit:
            for b in scan_kit:
                if (a == 0) and (b == 0):
                    pass
                else:
                    temp_x = i+a
                    temp_y = j+b
                    test = check_pixel(temp_x,temp_y)
                    if test == 1:
                        if im.getpixel((temp_x,temp_y)) == 255:
                            cnt += 1
        if (cnt == 8):
            im.putpixel((i,j),255)
    return im
                    


def next_pointer(im,too_last_point,last_point,present_point):
    global rows
    global cols

    next_point = None
    
    scan_kit = [-1,1,0]
    
    for i in scan_kit:
        for j in scan_kit:
            temp_point = [present_point[0]+i,present_point[1]+j]
            check0 = check_point(last_point,present_point,temp_point,too_last_point)
            if check0 == 1:
                if im.getpixel((temp_point[0],temp_point[1])) == 0:
                    next_point = temp_point
                    
    return next_point

def affine(im_ref,im):

    stop1 = stop(im_ref)
    stop2 = stop(im)

    theta = 0
    offset_x = 0
    offset_y = 0


    im_ref_line = segment_scan(im_ref,stop1)
    im_line = segment_scan(im,stop2)

    seq_len = 1

    for i in range(0,1):
        
        pnt_ref1 = im_ref_line[0]
        pnt_ref2 = im_ref_line[-1]
        
        pnt1 = im_line[0]
        pnt2 = im_line[-1]

        x1 = pnt_ref1[0]
        x2 = pnt_ref2[0]
        y1 = pnt_ref1[1]
        y2 = pnt_ref2[1]

        x1prime = pnt1[0]
        x2prime = pnt2[0]
        y1prime = pnt1[1]
        y2prime = pnt2[1]

        cosx = ((x2prime-x1prime)*(x2-x1) + (y2prime - y1prime)*(y2-y1))/((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))

        theta += arccos(cosx)

        offset_x += x1prime - (x1*cos(theta)-y1*sin(theta))
        offset_y += y1prime - (x1*sin(theta)+y1*cos(theta))

    theta = theta/seq_len
    offset_x = offset_x/seq_len
    offset_y = offset_y/seq_len

    return [[offset_x,offset_y],theta]
            
def segment_scan(im,point):

    last_point = point
    present_point = point
    pnt_cnt = 0

    scan_points = []
    kit = [-1,1,0]

    in_flag = 1
    while (pnt_cnt < 8):
        
        pnt_cnt = 0
        in_flag = 1
        
        for i in kit:
            for j in kit:
                temp_point = [present_point[0]+i,present_point[1]+j]
                check = check_point(last_point,present_point,temp_point,last_point)
                if check == 1:
                    if im.getpixel((temp_point[0],temp_point[1])) == 0:
                        print temp_point
                        last_point = present_point
                        present_point = temp_point
                        
                        scan_points.append([last_point[0],last_point[1]])
                        in_flag = 0
                        break
                    if in_flag == 0:
                        break
                pnt_cnt += 1     
    scan_points.append([present_point[0],present_point[1]])

    return scan_points 


def scan(im,resize = 1):
    
    name = namer_txt(im.filename)
    f = file(name,'wt')

    global rows
    global cols

    imout = bilevel(im)
    imout = edge_thick(imout,resize)
    imout = imout.resize((rows/resize,cols/resize))
    rows = rows/resize
    cols = cols/resize

    init_line = str(rows)+'x'+str(cols)+'\n'

    for i in init_line:
        f.write(i)
        
    imout = clean_t(imout)
    imout = unisolate(imout)

    flag0 = 1
    flag1 = 1
    cnt = 0
    while flag0:
        imout.save('sequence/segment'+str(cnt)+'.jpg')
        cnt+=1
        flag1 = 1
        print 'Starting new segment'
        init_point = stop(imout)
        if init_point == None:
            
            flag0 = 0
            break
                
        too_last_point = init_point
        last_point = init_point
        present_point = init_point
                
        while flag1:
                        
            imout.putpixel((last_point[0],last_point[1]),180)
            imout.putpixel((present_point[0],present_point[1]),180)
            temp_point = next_pointer(imout,too_last_point,last_point,present_point)
            
            if (temp_point == None) or (temp_point == init_point):
                flag1 = 0
                break
            else:
                too_last_point = last_point
                last_point = present_point
                present_point = temp_point
                
                coord_txt = str(present_point)+'\n'
                for i in coord_txt:
                    if i != ' ':
                        f.write(i)
        f.write('#')
    

rows = 0
cols = 0

