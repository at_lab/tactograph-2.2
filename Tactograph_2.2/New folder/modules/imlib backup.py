'''
    Version 0.3
    Now completely based only on scipy. PIL is eliminated due to speed issues
    and inefficient edge tracking.
'''

import Image
import ImageFilter
import sys
import os
import sift
from time import sleep

from scipy import *
from scipy.special import *
from scipy.signal import convolve2d
from scipy.linalg import *
from scipy.ndimage import *
from numpy.fft import fft2,ifft2,fftshift
import ransac
conv = convolve2d

class Register:
    '''
        Class for registering images based on FFT. The usage is as follows:
        >>> im0 = imread('image0.jpg', flatten = True)
        >>> im1 = imread('image1.jpg', flatten = True)
        >>> reg = Register(im0, im1)
        >>> shift = reg.shift
        >>> rotation = reg.theta

        Note:
            1. This image registration technique is not very reliable and
               is valid only for small rotation
            2. The class is very slow, since it depends on canny edge detection
               module for finding edges.
    '''
    def __init__(self,imin0, imin1, PROCESSED = False):
        '''
            This method is used to execute all the routines required to get the
            shift and the rotation
        '''
        # find edges to remove low frequency signals and suppress information
        if PROCESSED:
            im0 = imin0
            im1 = imin1
        else:
            im0 = Canny(imin0, 0.85, 5).grad
            im1 = Canny(imin1, 0.85, 5).grad

        # A major drawback of this method is that it can operate only on square
        # images. Hence we will make square image of any input image

        im0 = self.createsquareim(self.clearBorder(im0))
        im1 = self.createsquareim(self.clearBorder(im1))

        self.shift = self.findShift(im0,im1)
        imtrans = shift(im1, self.shift)
        # Remove the shift in the image. This is mandatory before we find theta
        impolar0 = self.makePolar(im0)
        impolar1 = self.makePolar(imtrans)
        self.index = self.findShift(impolar0, impolar1)[1]
        self.theta = ((self.index*90.0)/impolar1.shape[0])

    def clearBorder(self,im,width = 50, color = 255):
        '''
            A little house keeping to clear any border noise
        '''
        im[:,:width] = color
        im[:,-width:] = color
        im[:width,:] = color
        im[-width:,:] = color
        
        return im

    def createsquareim(self, im):
        """
        function createsquareim
        input:numpy ndarray
        output:numpy ndarray

        The function takes in an image array and converts it into square
        image by creating empty columns and rows.
        """
        lenmax = max(im.shape[0],im.shape[1])
        imout = zeros((lenmax,lenmax))
        imout[:,:] = 255
        imout[:im.shape[0],:im.shape[1]] = im
        return imout

    def findShift(self, im0, im1):
        '''
            This method is based on fft method of registering images.
        '''
        IM0 = fft2(im0)
        IM1 = fft2(im1)

        numer = IM0*conj(IM1)
        denom = abs(IM0*IM1)

        pulse_im = ifft2(numer/denom)
        mag = abs(pulse_im)
        x, y = where(mag == mag.max())
        
        x = array(x.tolist())   # Issues with read only arrays
        y = array(y.tolist())
        
        X, Y = im0.shape
        
        if x > X/2:
            x -= X
        if y > Y/2:
            y -= Y

        return [x[0], y[0]]

    def makePolar(self, im):
        '''
            This method will convert the cartesian coordinates image
            to polar coordinates image. The relation between the two
            domains is
              F(r,theta) = f(r*cos(theta),r*sin(theta))
            To make the process fast, we are using map_coordinates function
        '''
        m, n = im.shape
        r_max = hypot(m, n)
        
        r_mat = zeros_like(im)
        t_mat = zeros_like(im)

        r_mat.T[:] = linspace(0, r_max, m)
        t_mat[:] = linspace(0, pi/2, n)

        x = r_mat*cos(t_mat)
        y = r_mat*sin(t_mat)
        
        imout = zeros_like(im)
        imout = map_coordinates(im, [x, y], cval = 255)

        return imout

def siftRegister(im1, im2):
    '''
        Registering two images using the SIFT and RANSAC algorithm. The routines
        are in ransac.py

        im1 should be base image and im2 should be subsequent image
    '''
    try:
        os.mkdir(".temp")
    except OSError:
        pass
    #Image.fromarray(im1).convert('L').save('.temp/1.pgm')
    #Image.fromarray(im2).convert('L').save('.temp/12.pgm')
    im1.convert('L').save('.temp/1.pgm')
    im2.convert('L').save('.temp/2.pgm')

    ransac.process_image('.temp/1.pgm', '.temp/1.key')
    ransac.process_image('.temp/2.pgm', '.temp/2.key')

    key1 = sift.read_features_from_file('.temp/1.key')
    key2 = sift.read_features_from_file('.temp/2.key')

    match_scores = ransac.match(key1[1], key2[1])

    plist = ransac.get_points(key1[0], key2[0], match_scores)
    hmatrix = ransac.ransac(asarray(im1), asarray(im2), plist)
    H = inv(hmatrix)
    H[0][2] /= 20.0
    H[1][2] /= 20.0

    return hmatrix
    
def CreateFilter(rawfilter):
    """
    Function createfilter
    input: 1D list of the filter elements
    output:3x3 kernel of the elements

    This function creates the necessary filter for convolution with the image
    Note that the number of elements in the input list must be a perfect square
    """
    order = pow(len(rawfilter),0.5)
    rawarray = array(rawfilter)
    newfilter = rawarray.reshape(order,order)
    return newfilter

def TransformCoords(im0,im1,basecoords, PROCESSED):
    """
        Function TransformCoords
        input: base image im0, checked image im1 and the coordinates basecoords
        output: tranformed coordinates

        The function does the actual affine transformation
        It takes the images, finds the translation and rotation and applies
        it on the base coordinates using the following formula

        X = x0 + xcos(theta) - ysin(theta)
        Y = y0 + xsin(theta) + ycos(theta)

        Setting the PROCESSED flag will tell the Register function
        that the image is already processed and needs no further processing
    """
    
    if type(im0) != ndarray:
        im0 = array(asarray(im0).tolist())
    if type(im1) != ndarray:
        im1 = array(asarray(im1).tolist())
	
    reg = Register(im0, im1, PROCESSED)
        
    x0,y0 = reg.shift
    theta = reg.theta * ( pi / 180 )
    

    transcoords = [[]]

    for segment in basecoords:
        for point in segment:
            X = point[0]*cos(theta)-point[1]*sin(theta) + x0
            Y = point[0]*sin(theta)+point[1]*cos(theta) + y0
            transcoords[-1].append([X,Y])

        transcoords.append([])

    return transcoords

def TransformCoords2(im1, im2, basecoords):
    '''
        This is the second method to register two images using SIFT
    '''
    H = siftRegister(im1, im2)
    if type(im1) != ndarray:
        im1 = array(asarray(im1).tolist())
    if type(im2) != ndarray:
        im2 = array(asarray(im2).tolist())

    transcoords = []

    for segment in basecoords:
        for point in segment:
            x0, y0 = point
            # The order of x0, y0 and x, y is not an error.
            # This is to keep consistency between PIL images and
            # ndimage arrays
            A = array([y0, x0, 1]).reshape(3,1)
            y, x, z = dot(H, A)
            transcoords[-1].append([x[0], y[0]])
        transcoords.append([])

    return transcoords

def Namefile(imname):
    """
        Function Namefile
        input: image name string
        output: coordinates file name string

        A very simple function to return the name of the file to be created
        to hold the coordinates.
    """
    halt = imname.index('.')
    imagename = imname[:halt]
    filename = imagename+'SegmentFile.cfile'
    return filename

def ClearThinEdges(im,thres = 1):
    """
        function ClearThinEdges
        input : image array im,threshold
        output : image array

        The function takes image array as input and clears all the thin edges.
        The threshold decides the removal of edges. Normally, anything
        other than 1 may completely wipe off the image

        The image is convolved with the following averaging kernel:
        [0,1,0,
         1,4,1,
         0,1,0]
        The image size will increase by two, in both the dimensions. But this is
        taken care by taking the inner symmetric slice.
    """
    avkernel = CreateFilter([0,1,0,
                             1,0.5,1,
                             0,1,0])
    for iters in range(thres):
        imout_temp = real(convolve2d(im,avkernel))
    imout = ones_like(im)
    imout = imout_temp[1:-1,1:-1]
    return imout

def check_pixel(x,y):
    """
        Function check_pixel
        input : pixel positions x,y
        output: boolean

        The function takes the pixel position and returns true if it is within
        the image and false if not. This function is used by the SegmentScan
        and the EdgeScan function.
    """

    global rows,cols
    
    if rows == 0 or cols == 0:
        print 'internal global values rows and cols must be initialised'
        sys.exit('Initialisation error')

    if (x > rows-1) or (y > cols-1) or (x < 0) or (y < 0):
        return False
    else:
        return True

def check_point(lastpoint,presentpoint,temppoint,too_lastpoint):
    """
        Function check pixel
        input: lastpoint,presentpoint,temppoint,too_lastpoint
        output: boolean
        
        The function is used by SegmentScan and EdgeScan and should never be
        used standalone.
        It checks if the presently scanned pixel is not any of the last
        points already scanned or it is a point outside the image
    """
    global rows
    global cols

    check0 = check_pixel(temppoint[0],temppoint[1])
    if check0 == True:
        if (temppoint != lastpoint) and (temppoint != presentpoint):
            if temppoint != too_lastpoint:
                return True
            else:
                return False

def stop(im,val = 0):
    """
        Function stop
        input: 2D numpy ndarray
        output: xy tuple

        This function returns the coordinates of the first black pixel encountered.
    """
    X,Y = where(im == val)
    try:
        y = Y.min()
    except:
        return None
    X = X.tolist()
    Y = Y.tolist()
    index = Y.index(y)
    x = X[index]
    return [x,y]


def bilevel(imarray,thres = 200):
    """
        Function bilevel
        input: numpy ndimage
        output: numpy ndimage

        This function simply performs a bilevel conversion operation

        Remark: This function may get depricated
    """

    global rows
    global cols

    if type(imarray) != ndarray:
        imarray = asarray(imarray).tolist()
        imarray = array(imarray)

    if rows == 0 or cols == 0:
        print 'internal global values rows and cols must be initialised'
        sys.exit('Initialisation error')
        
    x,y = where(imarray < thres)
    imarray[:,:] = 255
    imarray[x,y] = 0

    return imarray

def edge_thick(im,box_size = 3):
    """
        Function edge_thick
        input: 2D numpy ndarray
        output: 2D numpy ndarray

        this function simply increases the thickness of the edges in
        the given image
    """
    
    k = int(box_size / 2)
    X, Y = im.shape
    for i in range(k):
        x,y = where(im == 0)
        im[x,y+1] = 0
        im[x+1,y] = 0
        im[x,y-1] = 0
        im[x-1,y] = 0
        im[x+1,y+1] = 0
        im[x-1,y-1] = 0
        im[x-1,y+1] = 0
        im[x+1,y-1] = 0

    return im

def tclean(im):
    """
        Function tclean
        input: 2D numpy ndarray
        output: 2D numpy ndarray

        The function perform a 't' cleaning. That is if the following type of
        pixel grid if found:
         _              _
        |_|_          _|_|_
        |_|_|  or    |_|_|_|
        |_|
          ,then, the projecting pixels are removed. This ensures cleaner edges
    """
    global rows
    global cols
    
    tkit1 = [[0,0],[1,0],[1,-1],[1,1]]
    tkit2 = [[0,0],[-1,0],[-1,-1],[-1,1]]

    for i in range(rows):
        for j in range(cols):
            cntxr = 0
            cntxl = 0
            cntyr = 0
            cntyl = 0

            for h in tkit1:
                p = i+h[0]
                q = j+h[1]

                test = check_pixel(p,q)
                if test == 1:
                    if im[p,q] == 0:
                        cntyr += 1

            for h in tkit2:
                p = i+h[0]
                q = j+h[1]

                test = check_pixel(p,q)
                if test == 1:
                    if im[p,q] == 0:
                        cntyl += 1

            for h in tkit1:
                p = i+h[1]
                q = j+h[0]

                test = check_pixel(p,q)
                if test == 1:
                    if im[p,q] == 0:
                        cntxr += 1

            for h in tkit2:
                p = i+h[1]
                q = j+h[0]

                test = check_pixel(p,q)
                if test == 1:
                    if im[p,q] == 0:
                        cntxl += 1
            
            if ((cntyr == 4) or (cntyl== 4) or (cntxr == 4) or (cntxl == 4)):
                im[i,j] = 255

    return im

def boxclean(im):
    """
        Function boxclean
        input: 2D numpy ndarray
        output: 2D numpy ndarray

        The function converts the following pixel grid:
         _ _           _            _
        |_|_|   into  |_|_    or  _|_|
        |_|_|           |_|      |_|
    """

    global rows
    global cols

    sqrkit = [0,1]
    
    for i in range(rows):
        for j in range(cols):
            cnt = 0
            for a in sqrkit:
                for b in sqrkit:
                    p = a+i
                    q = b+j
                    test = check_pixel(p,q)
                    if test:
                        if im[i,j] == 0:
                            cnt += 1
            flaglt = 0
            flagrt = 0
            flaglb = 0
            flagrb = 0
            if cnt == 4:
                
                left_app_coords = [[i,j-1],[i-1,j-1],[i-1,j],[i+2,j+1],[i+2,j+2],[i+1,j+2]]
                
                right_app_coords = [[i+1,j-1],[i+2,j-1],[i+2,j],[i-1,j+1],[i-1,j+2],[i,j+2]]
                                
                for point in left_app_coords:
                    x = point[0]
                    y = point[1]
                    
                    test = check_pixel(x,y)
                    if test == 1:
                        
                        if im[i,j] == 0:
                            flag = 1                            

                for point in right_app_coords:
                    x = point[0]
                    y = point[1]

                    test = check_pixel(x,y)
                    if test == 1:
                        if im[i,j] == 0:
                            flag = 2
                            
                if (flag == 1):
                    
                    im[i,j+1] = 255
                    im[i+1,j] = 255
                if (flag == 2):
                    
                    im[i,j] = 255
                    im[i+1,j+1] = 255
            
    return im

def unisolate(im):
    """
        Function unisolate
        input: PIL image
        output: PIL image

        This function removes any stray black pixels in the image.
        May be necessary after the tclean and the boxclean operations.
    """
    rows,cols = im.shape
        
    black_coords = [];
    for i in range(rows):
        for j in range(cols):
            if im[i,j] == 0:
                black_coords.append([i,j])

    for point in black_coords:
        i = point[0]
        j = point[1]
        cnt = 0
        scan_kit = [-1,0,1]
        for a in scan_kit:
            for b in scan_kit:
                if (a == 0) and (b == 0):
                    pass
                else:
                    temp_x = i+a
                    temp_y = j+b
                    test = check_pixel(temp_x,temp_y)
                    if test:
                        if im[temp_x,temp_y] == 255:
                            cnt += 1

        if (cnt == 7) or (cnt == 6):
            im[i,j] = 255
    return im


def NextPoint(im,p2,p1,p0):
    """
        Function NextPoint
        input: PIL image im,previous points
        output: possible next point in the segment

        The function creates a 3x3 kernel around the present pixel
        and searches for the next black pixel.
        This function is used by SegmentScan and EdgeScan and must not be used
        as a standalone function.
    """
    kit = [-1,0,1]
    X,Y = im.shape
    for i in kit:
        for j in kit:
            if (i+j) == 0:
                continue
            x = p0[0]+i
            y = p0[1]+j
            
            if (x<0) or (y<0) or (x>=X) or (y>=Y):
                continue
            if ([x,y] == p1) or ([x,y] == p2):
                continue
            if (im[x,y] == 0):
                return [x,y]
    return None

class Canny:
    '''
        Create instances of this class to apply the Canny edge
        detection algorithm to an image.

        input: imagename(string) or image matrix,sigma for gaussian blur
        optional args: thresHigh,thresLow

        output: numpy ndarray.

        P.S: use canny.grad to access the image array        

        Note:
        1. Large images take a lot of time to process, Not yet optimised
        2. thresHigh will decide the number of edges to be detected. It
           does not affect the length of the edges being detected
        3. thresLow will decide the lenght of hte edges, will not affect
           the number of edges that will be detected.

        usage example:
        >>>canny = Canny('image.jpg',1.4,50,10)
        >>>im = canny.grad
        >>>Image.fromarray(im).show()
    '''
    def __init__(self,imname,sigma,window = 5,thresHigh = 50,thresLow = 10):
        try:
            self.imin = imread(imname,flatten = True)
        except:
            self.imin = imname

        # Create the gauss kernel for blurring the input image
        # It will be convolved with the image
        gausskernel = self.gaussFilter(sigma,window)
        # fx is the filter for vertical gradient
        # fy is the filter for horizontal gradient
        # Please not the vertical direction is positive X
        
        fx = self.createFilter([1, 1, 1,
                                0, 0, 0,
                               -1,-1,-1])
        fy = self.createFilter([-1,0,1,
                                -1,0,1,
                                -1,0,1])

        imout = conv(self.imin,gausskernel)[1:-1,1:-1]
        gradx = conv(imout,fx)[1:-1,1:-1]
        grady = conv(imout,fy)[1:-1,1:-1]

        # Net gradient is the square root of sum of square of the horizontal
        # and vertical gradients

        grad = hypot(gradx,grady)
        theta = arctan2(grady,gradx)
        theta = 180 + (180/pi)*theta
        # Only significant magnitudes are considered. All others are removed
        x,y = where(grad < 10)
        theta[x,y] = 0
        grad[x,y] = 0

        # The angles are quantized. This is the first step in non-maximum
        # supression. Since, any pixel will have only 4 approach directions.
        x0,y0 = where(((theta<22.5)+(theta>157.5)*(theta<202.5)
                       +(theta>337.5)) == True)
        x45,y45 = where( ((theta>22.5)*(theta<67.5)
                          +(theta>202.5)*(theta<247.5)) == True)
        x90,y90 = where( ((theta>67.5)*(theta<112.5)
                          +(theta>247.5)*(theta<292.5)) == True)
        x135,y135 = where( ((theta>112.5)*(theta<157.5)
                            +(theta>292.5)*(theta<337.5)) == True)

        self.theta = theta
        self.theta[x0,y0] = 0
        self.theta[x45,y45] = 45
        self.theta[x90,y90] = 90
        self.theta[x135,y135] = 135

        self.grad = grad.copy()
        x,y = self.grad.shape

        for i in range(x):
            for j in range(y):
                if self.theta[i,j] == 0:
                    test = self.nms_check(grad,i,j,1,0,-1,0)
                    if not test:
                        self.grad[i,j] = 0

                elif self.theta[i,j] == 45:
                    test = self.nms_check(grad,i,j,1,-1,-1,1)
                    if not test:
                        self.grad[i,j] = 0

                elif self.theta[i,j] == 90:
                    test = self.nms_check(grad,i,j,0,1,0,-1)
                    if not test:
                        self.grad[i,j] = 0
                elif self.theta[i,j] == 135:
                    test = self.nms_check(grad,i,j,1,1,-1,-1)
                    if not test:
                        self.grad[i,j] = 0
                        
        init_point = self.stop(self.grad, thresHigh)
        # Hysteresis tracking. Since we know that significant edges are
        # continuous contours, we will exploit the same.
        # thresHigh is used to track the starting point of edges and
        # thresLow is used to track the whole edge till end of the edge.
        
        while (init_point != -1):
            
            self.grad[init_point[0],init_point[1]] = -1
            p2 = init_point
            p1 = init_point
            p0 = init_point
            p0 = self.nextNbd(self.grad,p0,p1,p2,thresLow)
            
            while (p0 != -1):
                #print p0
                p2 = p1
                p1 = p0
                self.grad[p0[0],p0[1]] = -1
                p0 = self.nextNbd(self.grad,p0,p1,p2,thresLow)
                
            init_point = self.stop(self.grad,thresHigh)

        # Finally, convert the image into a binary image
        x,y = where(self.grad == -1)
        self.grad[:,:] = 0
        self.grad[x,y] = 255
            

    def createFilter(self,rawfilter):
        '''
            This method is used to create an NxN matrix to be used as a filter,
            given a N*N list
        '''
        order = pow(len(rawfilter),0.5)
        order = int(order)
        filt_array = array(rawfilter)
        outfilter = filt_array.reshape((order,order))
        return outfilter
    
    def gaussFilter(self,sigma,window = 3):
        '''
            This method is used to create a gaussian kernel to be used
            for the blurring purpose. inputs are sigma and the window size
        '''
        kernel = zeros((window,window))
        c0 = window // 2

        for x in range(window):
            for y in range(window):
                r = hypot((x-c0),(y-c0))
                val = (1.0/2*pi*sigma*sigma)*exp(-(r*r)/(2*sigma*sigma))
                kernel[x,y] = val
        return kernel / kernel.sum()

    def nms_check(self,grad,i,j,x1,y1,x2,y2):
        '''
            Method for non maximum supression check. A gradient point is an
            edge only if the gradient magnitude and the slope agree

            for example, consider a horizontal edge. if the angle of gradient
            is 0 degress, it is an edge point only if the value of gradient
            at that point is greater than its top and bottom neighbours.
        '''
        try:
            if (grad[i,j] > grad[i+x1,j+y1]) and (grad[i,j] > grad[i+x2,j+y2]):
                return 1
            else:
                return 0
        except IndexError:
            return -1
    
    def stop(self,im,thres):
        '''
            This method is used to find the starting point of an edge.
        '''
        X,Y = where(im > thres)
        try:
            y = Y.min()
        except:
            return -1
        X = X.tolist()
        Y = Y.tolist()
        index = Y.index(y)
        x = X[index]
        return [x,y]
  
    def nextNbd(self,im,p0,p1,p2,thres):
        '''
            This method is used to return the next point on the edge.
        '''
        kit = [-1,0,1]
        X,Y = im.shape
        for i in kit:
            for j in kit:
                if (i+j) == 0:
                    continue
                x = p0[0]+i
                y = p0[1]+j
                
                if (x<0) or (y<0) or (x>=X) or (y>=Y):
                    continue
                if ([x,y] == p1) or ([x,y] == p2):
                    continue
                if (im[x,y] > thres):
                    return [x,y]
        return -1

def SegmentScan(im,point):
    """
        Function SegmentScan
        input: PIL image, xy tuple
        output: list

        This functions scans the edges of the image from the point 'point',
        till 8 points are scanned. It returns the list of the
        black point scanned. The function may be used for affine transform
        when a cross bar is provided at the top of the image.

        This function may soon be depricated.
    """
    last_point = point
    present_point = point
    pnt_cnt = 0

    scan_points = []
    kit = [-1,1,0]

    in_flag = 1
    while (pnt_cnt < 8):
        
        pnt_cnt = 0
        in_flag = 1
        
        for i in kit:
            for j in kit:
                temp_point = [present_point[0]+i,present_point[1]+j]
                check = check_point(last_point,present_point,temp_point,last_point)
                if check == 1:
                    if im.getpixel((temp_point[0],temp_point[1])) == 0:
                        #print temp_point
                        last_point = present_point
                        present_point = temp_point
                        
                        scan_points.append([last_point[0],last_point[1]])
                        in_flag = 0
                        break
                    if in_flag == 0:
                        break
                pnt_cnt += 1     
    scan_points.append([present_point[0],present_point[1]])

    return scan_points 

def EdgeScan(im):
    """
        Function EdgeScan
        input: 2D numpy ndarray,resize value
        output: 2 dimensional array of segment coordinates.

        The function scans all the edges in the given image and creates a list
        of the segment coordinates. A very general function. It used a 3x3 kernel
        for the scanning purpose. It automatically creates a file with the same
        name as the image.
        
        Note 1: input image must previously be cleaned
        
        Note 2: the spacing of the points is not appropriate for the TactoGraph
        to plot. The final integration module has the function CreateAnchor to
        take care of this.
    """

    global imname
    name = Namefile(imname)
    f = file(name,'wt')

    global rows
    global cols

    im[:,0] = 255
    im[:,-1] = 255
    im[0,:] = 255
    im[-1,:] = 255
    
    imout = im
    init_line = '#'+str(cols)+'x'+str(rows)+'\n'

    f.writelines(init_line)
    flag0 = 1
    flag1 = 1
    cnt = 0
    while flag0:
        cnt+=1
        flag1 = 1
        init_point = stop(imout)        
        print 'Starting new segment at',init_point
        if init_point == None:
            flag0 = 0
            break
                
        too_last_point = init_point
        last_point = init_point
        present_point = init_point
        
        f.writelines(str([init_point[1],init_point[0]])+'\n')
                
        while flag1:
            imout[last_point[0],last_point[1]] = 180
            imout[present_point[0],present_point[1]] = 180
            
            temp_point = NextPoint(imout,too_last_point,last_point,present_point)
            
            if (temp_point == None) or (temp_point == init_point):
                flag1 = 0
                break
            else:
                too_last_point = last_point
                last_point = present_point
                present_point = temp_point
                
                coord_txt = str([present_point[1],present_point[0]])+'\n'
                for i in coord_txt:
                    if i != ' ':
                        f.write(i)
        f.write('%new segment%\n')

class Link:
    '''
        Create instances of this class to link broken segments of the edges
        detected. The threshold value is the distance, within which the edges
        will be linked together.

        Use link.segments to access the new segments list
        To check the reduction in edges, use link.NUM_OLD_EDGES and
        link.NUM_NEW_EDGES

        Note: The linkThres is ideally 2.1 (tried and tested)
    '''
    def __init__(self,segments,linkThres):
        self.segments = segments
        self.NUM_OLD_EDGES = len(segments)
        self.thres = linkThres
        self.segments = filter(self.empty,self.segments)
        
        k = len(self.segments)
        popout = []
        for i in range(k-1):
            for j in range(i+1,k):
                s0 = self.segments[i]
                s1 = self.segments[j]

                t0 = self.dist(s0[0],s1[0])
                t1 = self.dist(s0[-1],s1[-1])
                t2 = self.dist(s0[0],s1[-1])
                t3 = self.dist(s0[-1],s1[0])

                if t0 < self.thres:
                    popout.append(self.segments.index(s1))
                    self.segments[j].reverse()
                    self.segments[i] = self.segments[j]+self.segments[i]
                elif t1 < self.thres:
                    popout.append(self.segments.index(s1))
                    self.segments[j].reverse()
                    self.segments[i] = self.segments[i]+self.segments[j]
                elif t2 < self.thres:
                    popout.append(self.segments.index(s1))
                    self.segments[i] = self.segments[j]+self.segments[i]
                elif t3 < self.thres:
                    popout.append(self.segments.index(s1))
                    self.segments[i] = self.segments[i]+self.segments[j]
        for i in popout:
            self.segments[i] = 0
        self.segments = filter(None,self.segments)
        self.NUM_NEW_EDGES = len(self.segments)

    def dist(self,p0,p1):
        '''
            This method returns the distance between the two points
            p0 and p1
        '''
        x0,y0 = p0
        x1,y1 = p1
        length = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1)
        return pow(length,0.5)

    def empty(self,data):
        '''
            This method removes the empty edges in the segments.
        '''
        if data == []:
            return False
        else:
            return True

class webcam:
    """
        Class webcam

        Create instances of this class to access the processed image
        from the webcam

        Note 1: edge detection Threshold is an optional value. Default is 4
        Note 2: This class depends on the VideoCapture module.
                It works in windows but there is no support yet for
                linux. A possible work around is to use gstreamer.
    """
    def __init__(self,sigma,window,Tl,Th):
        """
            All the initialisation is done here.
            It is assumed that you have an auxilary camera or a builtin
            No error check for no camera or more than 2 cameras

            use webcam.image for the final output image.
        """
        self.sigma = sigma
        self.window = window
        self.Tl = Tl
        self.Th = Th

        if 'win' in sys.platform:
            self.imin = self.WinWebCam()
        elif 'linux' in sys.platform:
            self.imin = self.LinuxWebCam()
            
        global rows,cols,imname
        imname = 'cam.jpg'
        self.imin = self.imin.convert('L')
        self.imA4 = self.CreateA4(self.imin)
        self.guide = zoom(self.imA4, 2)
        rows,cols = self.imA4.size
        self.image = self.CreateEdges(self.imA4)
        self.guide_outline = zoom(self.image, 2)
        
    def CreateA4(self,im):
        """
            Use this method to create a 298x210 image from a 640x480 image

            Note: This method will not distort the image. It maintains the
                  pixel aspect ratio. It adds 18 blank columns to the
                  input image, zoomed 7/16 times.
        """
        imarray = asarray(im)
        imarray = zoom(imarray,7.0/16.0)
        imout_raw = zeros((210,298))
        imout_raw[:,:] = 255
        imout_raw[:,:-18] = imarray
        imout = Image.fromarray(imout_raw)
        return imout
    
    def CreateEdges(self,im):
        """
            This method creates the final image for the necessary edge follow
            algorithm.
                The functions tclean and unisolate make this method a little slow
                future version may take over this problem.
        """
        canny = Canny(im,self.sigma,self.window,self.Th,self.Tl)
        edgemap = canny.grad
        edges = 255 - edgemap
        return edges

    def WinWebCam(self):
        '''
            This method is used to capture image in the windows operating
            system. This method requires VideoCapture module
        '''

        print 'In Windows operating system. Will use VideoCapture module'

        import VideoCapture
        try:
            cam = VideoCapture.Device(0)
        except:
            print 'No secondary camera found. Will use the first webcam found'
            cam = VideoCapture.Device(1)

        im = cam.getImage()
        return im

    def LinuxWebCam(self):
        '''
           This method is used to capture image in the posix operating system.
           Requires v4l2capture module
        '''

        print 'In posix operating system. Will use v4l2capture module'

        import v4l2capture as capture
        import select

        try:
            cam = capture.Video_device('/dev/video1')
        except:
            print 'No secondary camera. Will use dev/video0'
            cam = capture.Video_device('/dev/video0')

        x,y = cam.set_format(640,480)
        cam.create_buffers(1)
        cam.queue_all_buffers()
        cam.start()
        select.select((cam,),(),())
        try:
            imdat = cam.read()
        except IOError:
            print 'Retrying to load image'
            sleep(1)
            imdat = cam.read()
        cam.close()
        im = Image.fromstring('RGB',(x,y),imdat)

        return im
            
rows = 0
cols = 0
imname = None
