############bugs fixed in Tactograph 2.1#################
1) Backend Error(fixed)
2) Camera Light adjustment(fixed)
3) Making the Tactograph to work with Windows 10(fixed)  
4) Spill of all fluid while printing, occurs if FAST EXTRUSION MODE is ON(Fixed)
5) Suppose unexpectedly if the manual motor window is closed without the stopping the functions of extruder motor, it keeps rotating.(Fixed)
