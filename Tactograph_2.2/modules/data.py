import os
from math import cos,sin

data_xy = []
data_theta = []

def irnd(num):
    y = round(num)-0.5
    return int(y) + ( y > 0)
    
def trunc(f, n):
    """
        Function trunc
        input: float value and number of digits
        output: truncated float.
        
    """
    return ('%.*f' % (n + 1, f))[:-1]


def retrieve_theta(coord):
    """
        Function retrieve_theta
        input: coordinate list
        output: angle list corresponding to the input coordinate.
    """
    for i in data_xy:
        if i == coord:
          return data_theta[data_xy.index(i)]

def retrieve_xy(angles):
    """
        Function retrieve_xy
        input: angles list
        output: coordinate list corresponding to the input angles
    """
    for i in data_theta:
        if i == angles:
            return data_xy[data_theta.index(i)]

def test_x(x_coord):
    """
        This function checks if the given x coordinate has matching
        y coordinates

        Function is now depricated
    """
    y_coord_count = 0
    for i in data_xy:
        if i[0] == x_coord:
            y_coord_count += 1
            print "Cordinate found:",'(',i[0],',',i[1],')'
    print y_coord_count,"y coordinates availabe"
    

def test_y(y_coord):
    """
        This function checks if the given y coordinate has matching
        x coordinates

        Function is now depricated
    """
    for i in data_xy:
        if i[1] == y_coord:
            print "Coordinate found:",'(',i[0],',',i[1],')'

def test_theta(Motor_1_angle):
    """
        This function checks if the given motor 1 angle has matching
        motor 2 angle

        Function is now depricated
    """
    alpha = 0
    for i in data_theta:
        if i[0] == Motor_1_angle:
            alpha = alpha + 1
            print"Angle match found:",i[0],i[1]
    print alpha," Angle matches found"
 
# Configuring the interrupts is a little tricky and needs some
# trial and error. However, to make things simple, follow the steps:
# 1. If a is sent, the right motor as seen from top should turn clockwise
# 2. c should turn the right motor anticlockwise
# 3. f should turn the left motor anticlockwise
# 4. r should turn the left motor clockwise
def sequencer(steps_1 , steps_2):
    """
        Function sequencer
        input: number of steps for first motor,number of steps for second motor
        output: list of interrupt sequence

        This function generates sequence based on most uniform
        distribution of the interrupts

        This is a backend function, should not be used stand alone.
    """
    if steps_1 > 0:
        interrupt_1 = 'c'
    elif steps_1 < 0:
        interrupt_1 = 'a'
        steps_1 = 0 - steps_1
   
    sequence = []    
    temp_interr = 0
    temp_step = 0
    
    if steps_2 > 0:
        interrupt_2 = 'r'
    elif steps_2 < 0:
        interrupt_2 = 'f'
        steps_2 = 0 - steps_2
    
    if steps_1 == 0:
        while steps_2:
            sequence.append(interrupt_2)
            steps_2 -= 1
    elif steps_2 == 0:
        while steps_1:
            sequence.append(interrupt_1)
            steps_1 -= 1
    else:

        if steps_1 > steps_2 or steps_1 == steps_2:
            pass
        else:
            temp_interr = interrupt_1
            interrupt_1 = interrupt_2
            interrupt_2 = temp_interr

            temp_step = steps_1
            steps_1 = steps_2
            steps_2 = temp_step
            
        embed_factor = irnd(steps_1/steps_2)
        rem_interrupts = steps_1 - (steps_2 - 1)*embed_factor
    
        seq_begin = irnd((rem_interrupts)/2)
        seq_end = rem_interrupts - seq_begin
        
        while seq_begin:
            sequence.append(interrupt_1)
            seq_begin -= 1

        total_cnt = steps_2
        
        while total_cnt :
        
            temp = embed_factor
            sequence.append(interrupt_2)
            while temp:
                sequence.append(interrupt_1)
                temp -= 1
            total_cnt -= 1
        dif_temp = seq_end - embed_factor
        
        if dif_temp > 0:
            while dif_temp:
                sequence.append(interrupt_1)
                dif_temp -=1
        elif dif_temp < 0:
            cntr_seq = len(sequence)
            while dif_temp:
                  
                sequence.pop()
                
                dif_temp += 1
        else:
            pass
    
    return sequence

def line_prescaler(point1,point2):
    """
        Function line_prescaler
        input: terminal points
        output: set of points

        This function divides the line into set of uniformly
        spaced points and returns them.

        This function may soon be depricated
    """
    x1 = point1[0]
    x2 = point2[0]
    y1 = point1[1]
    y2 = point2[1]

    dist = pow((pow((x2 - x1),2)+pow((y2-y1),2)),0.5)
    prescaler = irnd(dist) * 1

    inter_coords = []
    inter_coords.append(point1)
    for i in range(0,prescaler):
        if i == 0:
            pass
        else:
            newx = (x1*(prescaler - i) + x2*i)/prescaler
            newy = (y1*(prescaler - i) + y2*i)/prescaler
            new_x = float(trunc(newx,3))
            new_y = float(trunc(newy,3))
            inter_coords.append(point_approximator(inter_coords[-1],[new_x,new_y]))
    print prescaler," points created"
    inter_coords.append(point2)
    return inter_coords

def point_approximator_base(point0,point,thresh):
    """
        Function is now depricated
    """
    x = point[0]
    y = point[1]
    flag = 0
    ref_angle = retrieve_theta(point0)
    for ref in data_xy:
        if ref == point:
            return ref
            flag = 2
        else:
            flag =3
            
    if flag == 3:
        for ref in data_xy:
            dif_x = ref[0] - x
            dif_y = ref[1] - y
            
            dist_point = pow((pow(dif_x,2)+pow(dif_y,2)),0.5)
            if dist_point < thresh and (abs(data_theta[data_xy.index(ref)][0]  - ref_angle[0]) < 20 ) and (abs(data_theta[data_xy.index(ref)][1] - ref_angle[1] < 20)):
            
                return ref
               
def point_approximator(point0,point):
    """
        function is now depricated
    """
    flag = 1
    thres = 0.05
    while flag == 1:
        temp = point_approximator_base(point0,point,thres)
        if temp is None:
            flag = 1
            thres += 0.05
        else:
            return temp

def abs_point_approx_base(point,thresh):
    """
        function is now depricated
    """
    x = point[0]
    y = point[1]
    
    flag = 0
    
    for ref in data_xy:
        if ref == point:
            return ref
            flag = 2
        else:
            flag =3
            
    if flag == 3:
        for ref in data_xy:
            dif_x = ref[0] - x
            dif_y = ref[1] - y
            
            dist_point = pow((pow(dif_x,2)+pow(dif_y,2)),0.5)
            
            if dist_point < thresh:            
                return ref
               
def abs_point_approx(point):
    """
        function is now depricated
    """
    flag = 1
    thres = 0.05
    while flag == 1:
        temp = abs_point_approx_base(point,thres)
        if temp is None:
            flag = 1
            thres += 0.05
        else:
            return temp

def approx(point):
    """
        Function approx
        input: xy tuple
        output: xy tuple corresponding to nearest existing point
        
    """
    global data_xy

    dists = []
    approx_point = None

    for ref in data_xy:
        dist = pow((point[0]-ref[0]),2) + pow((point[1]-ref[1]),2)
        dists.append(dist)

    approx_point = data_xy[dists.index(min(dists))]

    return approx_point

def extractor(str_val):
    """
        Function extractor
        input: string form of tuple
        output: tuple of xy coordinates

        A simple function to return float data from string data
    """
    halt = str_val.index(',')
    x = 0
    y = 0

    x = float(str_val[1:halt])
    try:
        y = float(str_val[halt+1:-2])
    except:
        y = float(str_val[halt+1:-3])
    
    return [x,y]
