'''
    Module: motor
    Interaction: USB device

    This module interacts directly with the PIC18F4550 device by sending the
    appropriate interrupts. This is the first layer of the device communication
    with respect to the device.

    The module depends on the pyusb module and the data module.
'''

__author__ = 'Vishwanath'
__all__ = ['motor_test','test0','test1','drawpixel','call','call_00',
           'call_0','interpolate','_draw','draw','goto','circle',]

import usb.core
from time import sleep,clock
from scipy import *

from math import atan,atan2,cos,pi
from data import trunc,cos,sin
from data import retrieve_theta, data_theta, data_xy
from data import retrieve_xy, sequencer, line_prescaler
from data import approx
from data import abs_point_approx_base,point_approximator_base
from data import point_approximator,abs_point_approx


dev = usb.core.find(idVendor=0x04D8,idProduct=0xFEAA)

step_interval = 0.020
H_SHIFT = 3.0
V_SHIFT = 1.4   
MOTOR_X = 11.5
MOTOR_Y = 32.5
LINK = 15

CAM_SHIFT = None
CAM_SCALE = None
CAM_ROTATION = None

if dev is None:
    print "Setup error.Device not found.Please check the connections."

else:
    dev.set_configuration()
    print "Found device."
    print dev.bDeviceClass , " Class device"
    print "Device ready for operation"

def irnd(num):
    y = round(num)-0.5
    return int(y) + ( y > 0)

def motor_test(interrupt):
    """
        Function motor_test
        input: interrupt
        output: None

        This function checks if the stepper motor completes a
        revolution successfully.

        Warning: Donot use this function if the motor is not in
              standalone position
    """
    steps = int(360.0 / step)
    for i in range(steps):
        call_0(interrupt)

def test0():
    """
        Function test0
        input: None
        output: None

        The function is to be used to check if individual motors are
        running as expected
    """
    motor_test('r')

def test1():
    """
        Function test1
        input: None
        output: None

        The function is to be used to check if individual motors are
        running as expected
    """
    motor_test('a')

def DrawPixel():
    """
        Function DrawPixel
        input: None
        output: None

        This function draws a distorted square connecting all the
        immediate neighbouring points
    """
    call_0('r')
    call_0('c')
    call_0('f')
    call_0('a')

def call(interrupt):
    """
        Function call
        input: interrupt character
        output: None

        Use this function to send 'interrupt' character to the USB device
    """
    try:
        dev.write(1,interrupt)
    except usb.core.USBError:
        sys.exit()

def call_00(motor1,motor2,time = step_interval):
    """
        Function call_00
        input: first interrupt character, second interrupt character,interval

        This function is used to simultaneously actuate both
        the stepper motors
    """
    
    call(motor1)
    sleep(time/100)
    call(motor2)
    sleep(time)

def call_0(motor,interval = step_interval):
    """
        Function call_0
        input: interrupt character,interval

        This function is used to actuate one of the stepper motors
    """
    
    call(motor)
    sleep(interval)

def liftPen():
    """
        Function to lift the plotter head up
    """
    call('d')
    
def dropPen():
    """
        Function to put the plotter pen down
    """
    call('u')

class interpolate2:
    '''
        New class for interpolating between two points
        The class now will depend less on the actual geometry of image
    '''
    def __init__(self,point1,point2):
        if point1 == point2:
            self.l2points = [point1]
        else:
            self.data = array(data_xy)
            self.x1,self.y1 = point1
            self.x2,self.y2 = point2
            self.l0points = self.prescale(point1,point2)
            self.l1points = map(self.p_approx,self.l0points)
            self.l2points = self.rmvDuplicates(self.l1points)

    def prescale(self,p1,p2):
        x1,y1 = p1
        x2,y2 = p2

        theta = atan2((y2-y1),(x2-x1))
        dist = (y2-y1)*(y2-y1) + (x2-x1)*(x2-x1)
        dist = pow(dist,0.5)

        scale = dist / 0.1
        print 'num points scaled: '+str(int(scale))

        r0 = (dist*1.0)/scale
        newpoints = []

        for i in range(0,irnd(scale+1)):
            x = x1 + r0*i*cos(theta)
            y = y1 + r0*i*sin(theta)
            newpoints.append([x,y])
            
        return newpoints

    def prescale2(self,p1,p2):
        x1,y1 = p1
        x2,y2 = p2

        dist = (y2-y1)*(y2-y1) + (x2-x1)*(x2-x1)
        dist = pow(dist,0.5)

        scale = dist / 0.1
        print 'num points scaled: '+str(int(scale))
        newpoints = []
        # Wherever possible DONOT use int, use irnd instead. int gives error
        for i in range(1,irnd(scale+1)):
            x = (x1*(scale-i)+i*x2)/scale
            y = (y1*(scale-i)+i*y2)/scale

            newpoints.append([x,y])
            
        return newpoints
        

    def rmvDuplicates(self,plist):
        newlist = []

        for i in plist:
            if i not in newlist:
                newlist.append(i)
        return newlist
    
    def p_approx(self,point):
        data = self.data
        p = array(point)
        dists = data - p
        dists *= dists
        dists = dists[:,0] + dists[:,1]
        x = where(dists == dists.min())

        return data[x][0].tolist()
        
class interpolate:
    '''
        Class interpolate

        This class creates points between two given points with least
        distance fitting

        The class incorporates three filters for creating accurate points.
        Use interpolate.outpoints for the final points list
        
    '''
    def __init__(self,point1,point2):
        '''
            __init__ simply calls all the functions and stores the
            points.
        '''
        self.x1,self.y1 = approx(point1)
        self.x2,self.y2 = approx(point2)

        print 'point1: ',point1
        print 'point2: ',point2

        self.L0points = filter(self.DistanceFilter,data_xy)
        self.L0points.remove([self.x1,self.y1])
        self.L0points.remove([self.x2,self.y2])
        self.L1points = filter(self.SlopeFilter,self.L0points)
        self.L2points = self.TwoPointFilter(self.L1points)
        self.L3points = filter(self.ExtensionFilter,self.L2points)
        self.outpoints = self.sort(self.L3points)
       
    def DistanceFilter(self,point,thres = 0.07):
        '''
            The DistanceFiter checks for the points that are at a
            given threshold on either side of the given line.

             However, it also creates points on either side of the
             extended line. ExtensionFilter takes care of this.
        '''
        x,y = point
        x1 = self.x1
        y1 = self.y1
        x2 = self.x2
        y2 = self.y2

        if x1 == x2:
            dist = abs(x-x1)
        elif y1 == y2:
            dist = abs(y-y1)
        else:
            numer = ((x - x1)/(x2 - x1)) + ((y1-y)/(y2-y1))
            denom = 1/pow((x2-x1),2) + 1/pow((y2-y1),2)
            dist = abs(numer)/pow(denom,2)
            
        if dist < thres:
            return True
        
    def SlopeFilter(self,point,angle = 9.0):
        """
            Since the points density is non-uniform, the slope filter
            will check for slope of the point w.r.t the starting and
            the ending point and will compare it with the slope of the
            line.
        """
        theta0 = pi + atan2(self.y2-self.y1,self.x2-self.x1)
        theta1 = pi + atan2(point[1]-self.y1,point[0]-self.x1)
        theta2 = pi + atan2(point[1]-self.y1,point[0]-self.x2)
        

        theta = min(theta1,theta2)
        
        if (abs(theta-theta0) < (pi/180)*angle) or ( abs(theta-theta0) > 360-(pi/180)*angle):
            return True

    def TwoPointFilter(self,points,angle = 15.0):
        """
            This filter ensures that the output points are not in a zigzag
            manner. It checks the inter points slope
        """
        output = [[self.x1,self.y1]]
        points.append([self.x2,self.y2])
        length = len(points)
        theta0 = pi + atan2( self.y2-self.y1,self.x2-self.x1)
        
        for i in range(length-1):
            x1,y1 = points[i]
            x2,y2 = points[i+1]

            theta = pi + atan2(y2-y1,x2-x1)
            
            if (abs(theta-theta0) < (pi/180)*angle) or ( abs(theta-theta0) > 360-(pi/180)*angle):
                output.append([x1,y1])
                
        output.append([self.x2,self.y2])                
        return output
    
    def ExtensionFilter(self,point):
        '''
            Removes all the points that are on the extended line
        '''
        x0,y0 = point
        x1 = self.x1
        y1 = self.y1
        x2 = self.x2
        y2 = self.y2

        if ( ( (x0>=x1) and (x0<=x2) ) or ((x0<=x1) and (x0>=x2)) ):
            if ( ( (y0>=y1) and (y0<=y2) ) or ((y0<=y1) and (y0>=y2)) ):
                return True
            else:
                return False
        else:
            return False

    def sort(self,points):
        '''
            This method sorts the points after level 4
        '''
        x0 = self.x1
        y0 = self.y1

        for p in points:
            dist = pow((p[0]-x0),2) + pow((p[1]-y0),2)
            dist = pow(dist,0.5)
            p.append(dist)
        for i in range(len(points)):
            for j in range(i,len(points)):
                if points[j][2] < points[i][2]:
                    temp = points[i]
                    points[i] = points[j]
                    points[j] = temp
        for p in points:
            p.pop()

        return points

def shift_correction(point, angle):
	'''
		This is a fallback method to find the shift.
		The input coordinates are for a pure pantograph.
		This function will calculate the necessary shift
	'''
	global H_SHIFT, V_SHIFT
	global MOTOR_X, MOTOR_Y
	global LINK
	theta1, theta2 = angle
	x, y = point
	phi = atan2( (MOTOR_Y - y + LINK * sin(theta1) ), (x - MOTOR_X) )
	
	x0 = x - H_SHIFT * cos(phi) - V_SHIFT * sin(phi)
	y0 = y - H_SHIFT * sin(phi) + V_SHIFT * cos(phi)
	
	return [int(x0*100) / 100.0, int(y0*100) / 100.0]

def _draw(point1,point2):
    """
        Function _draw
        input: starting and ending points
        output: None

        This function draws a line from point1 to point2, with the most uniform
        interrupt formation

        Note: _draw can be used directly only if the points are close enough
             Use draw for larger lines.
    """
    print 'Moving to '+str(point2)
    global step
    theta1 = retrieve_theta(point1)
    theta2 = retrieve_theta(point2)

    steps_1 = (theta2[0] - theta1[0])/step
    steps_2 = (theta2[1] - theta1[1])/step    
    final_flag = 1
    sequence = sequencer(irnd(steps_1),irnd(steps_2))
    cntr = len(sequence)
    
    cnt_loop = 0
    if cntr%2 == 0:
        final_flag = 0
        cntr_loop = cntr
    else:
        final_flag = 1
        cntr_loop = cntr - 1
        
    for i in range(0,cntr_loop,2):
        
        if sequence[i] == sequence[i + 1]:
            call_0(sequence[i])
            call_0(sequence[i + 1])            
        else:
            call_00(sequence[i + 1],sequence[i])            
        
    if final_flag == 1:
        call_0(sequence[-1])



def draw(point1,point2):
    """
        Function draw
        input: starting and ending points
        output: None

        This function is directly used by the goto function.
        It calls the _draw function with the interpolation.outpoints

        Note: This is the second version of the draw function
              The first version had a line divided into finite number
              of intermediate points. This function more relies on the
              geometry of the existing points rather than the geometry
              of the figure.
    """
    if point1 == point2:
        return
    interpoints = interpolate2(point1,point2).l2points
    for point in interpoints:
        _goto(point)

def _goto(point,distance_thres = 3):
    """
        Function _goto
        input: the point to which the plotter head must go
        output: None

        This function is used by draw utility. _goto uses _draw to go from point
        to point. Donot use this function standalone
    """
    global last_point
    x1 = last_point[0]
    x2 = point[0]
    y1 = last_point[1]
    y2 = point[1]
    
    point_approx = approx(point)    
    _draw(last_point,point_approx)
    last_point = point_approx

def goto(point,distance_thres = 3):
    """
        Function goto
        input: the point to which the plotter head must go
        output: None

        This is a frontend function. This function can be used directly
        for moving the plotter head

        Note: The last_point must be updated initially, since the control
              system is open loop.
    """
    global last_point
    x1 = last_point[0]
    x2 = point[0]
    y1 = last_point[1]
    y2 = point[1]

    point_approx = approx(point)    
    draw(last_point,point_approx)
    last_point = point_approx
    step_interval = 0.02

def circle(centre,radius,num_points = 20):
    """
        Function circle
        input: centre of circle, radius of circle and number of points(optional)

        This is a prototype function to draw a circle. May soon be depricated
    """
    x = centre[0]
    y = centre[1]
    global data_tuples
    goto(centre)
    step = int(360/num_points)
    points = []
    
    for i in range(0,361,step):
        temp_x = x + radius*cos(i*3.14159/180)
        temp_y = y + radius*sin(i*3.14159/180)
        temp_point = [float(trunc(temp_x,3)),float(trunc(temp_y,3))]
        
        points.append(temp_point)
    
    for ptr in points:
        goto(ptr)

#last point must be updated with the origin
last_point = [0,0]
