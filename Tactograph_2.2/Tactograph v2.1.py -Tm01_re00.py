#!/usr/bin/python2.7.6
# -*- coding: cp1252 -*-
from __future__ import division 
__author__ =  "Ikram Khan.S.I, Dinesh"
__credits__=  "Dr.Anil prabhakar, "
__version__ = "2.1"
__date__=     " April 2016"
__email__ =   "dineshch@enability.in"
__status__ =  "Complete prototype"
#######################Description #####################
'''
    This Tactograph application software is developed to establish a comunication between user and the tactile printr. The intreractive way of working is achived
    many advanced algorithms in background. The work of this program star ts from extablishing handshake between printer and pc. This application software opens a
    Graphical user interface, In which the image of tactograph print platorm will be captured and loaded for the user interface. To make the printing process easyier
    only two option(SHIFT + Mouse click, CTRL+mouse click) are use are used to draw the tactile outline. For more details on printing use help menu in GUI.
 
'''
#######################Notes#############################
'''
1.First the Tactograph need to be connected to PC, then run the application software.
'''

################Loding Modules###########################

import usb
import sys
import time
import math
# Modules for GUI constructions
import wx
import wx.lib.ogl as ogl
import wx.lib.agw.advancedsplash as AS
try:
    from modules import VideoCapture
except:
    from modules import v4l2capture
from modules import imlib as lib
import threading
from PIL import ImageEnhance
from PIL import Image as ress
from matplotlib import pyplot as plt
from modules import motor
import cv2
import numpy as np
from scipy import ndimage
from scipy.linalg import *
from scipy.special import *
from time import sleep,clock
import sys
import os

###########################################################
# Global constants.

NEW_POINT_RAD = 7# Radius of new point on the canvas
POINT_RAD = 7
H_MATRIX = None  #Camara homography matrix
H_mech = None    #Mechanical homography matrix
BaseImage = None
BaseCam = None
x_a4max=790      #Boundary step values form the home position
y_a4max=693
x_a4min=0
y_a4min=0
move=[]          #list used for storing the coordinates 
tflag=0          #Busy flag
d=6              #Start printing "d" steps before the start of the line along its slope
shade_e=0        #Variabls used to recoding shaded line segments
shadev=[]
shaden=[]

#####################################################

class processor():
    '''
    This class below is used for establishing communication between Tactograph and PC
    '''

    VENDOR = 0x04D8
    PRODUCT = 0xFEAA
    CONFIGURATION = 0x03 # if bootloader v2.x
    INTERFACE = 0
    ENDPOINT_IN = 0x82 # if bootloader v2.x
    ENDPOINT_OUT = 0x01
    device = None
    handle = None

    def __init__(self,):
        for bus in usb.busses():
            for dev in bus.devices:
                if dev.idVendor == self.VENDOR and dev.idProduct == self.PRODUCT:
                    self.device = dev
        return None

    def open(self):
        if not self.device:
            print >> sys.stderr, "Unable to find device!"
            return None
        try:
            self.handle = self.device.open()
            self.handle.setConfiguration(self.CONFIGURATION)
            self.handle.claimInterface(self.INTERFACE)
        except usb.USBError, err:
            print >> sys.stderr, err
            self.handle = None
        return self.handle

    def close(self):
        try:
            self.handle.releaseInterface()
        except Exception, err:
            print >> sys.stderr, err
        self.handle, self.device = None, None

    def read(self, length, timeout = 0):
        return self.handle.bulkRead(self.ENDPOINT_IN, length, timeout)

    def write(self, buffer, timeout = 0):
        return self.handle.bulkWrite(self.ENDPOINT_OUT, buffer, timeout)

class srManMotorCtrl(wx.Frame):
    '''
        This class help to creates manual system control with GUI. This can be used for refill
        of fluid and debugging the motor motion and  
    '''
    
    def __init__(self,parent,id):
        wx.Frame.__init__(self,parent,id,'Fluid refill and Tactograph test ',
                          style = wx.CLOSE_BOX |
                          wx.SYSTEM_MENU | wx.CAPTION)
        panel = wx.Panel(self)

        self.status = wx.StaticText(panel,size = (260,80),
                                    style = wx.BORDER_SUNKEN)        
        self.workStatus = '''Device ready for operation.
                    Motor 1 : $ steps
                    Motor 2 : # steps
                    Extruder status : @
                    Fast Extrusion: %
                    '''
        # Instance of the USB device.
        self.dev = motor.dev
        # Create a status bar for the GUI
        self.sb = self.CreateStatusBar()
        #  This Create the sizer for all buttons and message box, so that information will
        #be correctly aligned in GUI

        self.mBox1 = wx.StaticBox(panel, label = 'Extruder Refill')
        self.mBox2 = wx.StaticBox(panel, label = 'X axis motor control')
        self.ServoBox = wx.StaticBox(panel, label = 'Y axis motor control')
        self.motcontrol = wx.StaticBox(panel, label = 'Stepper control')

        sizer1 = wx.StaticBoxSizer(self.mBox1,orient = wx.HORIZONTAL)
        sizer2 = wx.StaticBoxSizer(self.mBox2,orient = wx.HORIZONTAL)
        sizer3 = wx.StaticBoxSizer(self.ServoBox,orient = wx.HORIZONTAL)
        sizermot= wx.StaticBoxSizer(self.motcontrol,orient = wx.HORIZONTAL)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.pos = 'Not moving'
        self.st=1
        self.posf = 'OFF' # variable to display on GUI
        self.stf=1          # variable used as a toggle switch
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        # Buttons for manual control. There are a total of 8 buttons,
        # X axis motor control: LEFT and RIGHT,
        # Y axis motor control: Forward and Reverse,
        # Extruder: up, down,Fast mode,lock extruder.
        
        M1_clk = wx.Button(panel,label = 'Forward',
                                pos = (10,10), size = (110,50))

        M1_aclk = wx.Button(panel,label = 'Reverse',
                                pos = (10,70), size = (110,50))

        M2_clk = wx.Button(panel,label = 'Left',
                                pos = (130,10), size = (110,50))

        M2_aclk = wx.Button(panel,label = 'Right',
                                pos = (130,70), size = (110,50))

                                
        Servo_up = wx.Button(panel,label = 'Extrusion Up',
                               size = (110,40))
        Servo_down = wx.Button(panel,label = 'Extrusion down',
                               size = (110,40))
        FServo_ctrl = wx.Button(panel,label = 'Fast\nExtrusion',
                               size = (110,40))
        Ext_lock = wx.Button(panel,label = 'Lock Extruder',
                               size = (110,40))
        
        #USB communication check
        if self.dev == None:
            self.status.SetLabel('Device not found.')
            self.sb.SetStatusText('Device not found. No functionality')
            M1_clk.Enable(False)
            M1_aclk.Enable(False)
            M2_clk.Enable(False)
            M2_aclk.Enable(False)
            Servo_ctrl.Enable(False)
            
        else:
            self.sb.SetStatusText('Device found.')
            self.step1 = 0
            self.step2 = 0
            self.ChangeStatus()
            self.dev.set_configuration()

        #sizers.
        sizer3.Add(M1_clk,1,wx.ALL,5)
        sizer3.Add(M1_aclk,1,wx.ALL,5)
        sizer2.Add(M2_clk,1,wx.ALL,5)
        sizer2.Add(M2_aclk,1,wx.ALL,5)
        sizer1.Add(Servo_up,1,wx.ALL,5)
        sizer1.Add(Servo_down,1,wx.ALL,5)
        sizer1.Add(FServo_ctrl,1,wx.ALL,5)
        sizer1.Add(Ext_lock,1,wx.ALL,5)
        sizermot.Add(sizer2,1,wx.ALL,5)
        sizermot.Add(sizer3,1,wx.ALL,5)
        sizer.Add(sizer1,1,wx.ALL,5)
        sizer.Add(sizermot,1,wx.ALL,5)
        sizer.Add(self.status)
        panel.SetSizer(sizer)
        sizer.Fit(self)
        self.Centre()
        #binding the functions
        M1_clk.Bind(wx.EVT_BUTTON,self.CallF)
        M1_aclk.Bind(wx.EVT_BUTTON,self.CallR)
        M2_clk.Bind(wx.EVT_BUTTON,self.CallA)
        M2_aclk.Bind(wx.EVT_BUTTON,self.CallC)
        Servo_up.Bind(wx.EVT_BUTTON,self.deextr)
        Servo_down.Bind(wx.EVT_BUTTON,self.extr)
        FServo_ctrl.Bind(wx.EVT_BUTTON,self.FastEXT)
        Ext_lock.Bind(wx.EVT_BUTTON,self.Extlock)      
        self.Show(True)
    # The next four methods are used for rotating the stepper motors. Note that
    # the exact mapping is not known. Needs to be updated still.
    def OnOpen(self,event):
        joinpoints.tra(100,100)
        
    def CallF(self,event):
        processor.write('2')
        suma=processor.read(1)#Ack
        self.step1 += 1
        self.ChangeStatus()
        
    def CallR(self,event):
        processor.write('8')
        suma=processor.read(1)
        self.step1 -= 1
        self.ChangeStatus()
        
    def CallA(self,event):
        processor.write('4')
        suma=processor.read(1)
        self.step2 += 1
        self.ChangeStatus()
        
    def CallC(self,event):
        processor.write('6')
        suma=processor.read(1)
        self.step2 -= 1
        self.ChangeStatus()
        
    def extr(self,event):
        processor.write('a')
        suma=processor.read(1)
        if(self.st==1):
            self.pos = 'down'
            self.st=0
        else:
            self.pos = 'Not moving'
            self.st=1
        self.ChangeStatus()

    def deextr(self,event):
        processor.write('q')
        suma=processor.read(1)
        if(self.st==1):
            self.pos = 'up'
            self.st=0
        else:
            self.pos = 'Not moving'
            self.st=1     
        
        self.ChangeStatus()
    def FastEXT(self,event):
        processor.write('5')
        suma=processor.read(1)
        if(self.stf==1):
            self.posf = 'ON'
            self.stf=0
        else:
            self.posf = 'OFF'
            self.stf=1
        self.ChangeStatus()    
    def Extlock(self,event):
        processor.write('5')
        suma=processor.read(1)
        processor.write('q')
        suma=processor.read(1)
        time.sleep(1.2)
        processor.write('q')
        suma=processor.read(1)
        processor.write('5')
        suma=processor.read(1)
        self.home()
    def OnClose(self,event):
        if(self.stf==0): # If on will off the Fast extrusion
            processor.write('5')
            suma=processor.read(1)
        if(self.st==0): # If on will off the extrusion
           processor.write('q')
           suma=processor.read(1) 
        self.home()
        self.Destroy()


    def home(self):
        processor.write('h')
        suma=processor.read(1)

    def ChangeStatus(self):
        temp = self.workStatus.replace('$',str(self.step1))
        temp = temp.replace('#',str(self.step2))
        temp = temp.replace('@',self.pos)
        temp = temp.replace('%',self.posf)
        self.status.SetLabel(temp)
        self.status.Size = (280,80)
        

class srCalibrate(wx.Frame):
    '''
        This Class shows a GUI window with a image of calibration sheet, The user click
        the four points for camera calibration.
    '''
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, "Calibrate virtual space")
        panel = wx.Panel(self)

        self.home()
        cam = lib.webcam(0.85, 5, 5, 30)    # Take a snapshot of the grid
        self.im = ress.open("bin/color.jpg")
        a,b=self.im.size
        a=int(1.5*a)
        b=int(1.5*b)
        self.size = a, b
        self.im_resized = self.im.resize(self.size,ress.ANTIALIAS)
        self.im_resized.save("bin/calibrate.bmp", "BMP")
        cal_image = wx.Bitmap("bin/calibrate.bmp")

        
        # Create the box sizers to hold the image,
        # info box and the universal sizer
        
        image_sizer = wx.BoxSizer(wx.HORIZONTAL)
        info_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer = wx.BoxSizer(wx.HORIZONTAL)
        control = wx.StaticBox(panel,label = 'Control Area')
        con_sizer = wx.StaticBoxSizer(control,orient = wx.VERTICAL)

        
        #Creating OGL system and embedding to GUI 
        
        canvas = ogl.ShapeCanvas(panel)
        diagram = ogl.Diagram()
        canvas.SetDiagram(diagram)
        diagram.SetCanvas(canvas)
        bg = ogl.BitmapShape()
        bg.SetBitmap(cal_image)
        bg.SetDraggable(False)
        xsize,ysize = cal_image.Size
        bg.SetX(int(xsize/2))
        bg.SetY(int(ysize/2))
        canvas.AddShape(bg)
        self.canv = canvas
        self.pos_info = wx.StaticText(panel,-1,
                                      'Mouse Position')
        
        # Create a button to help user tell that calibration is done
        
        done_button = wx.Button(panel, label = 'Done',size = (60,30))
        self.count = 0
        self.points = []
        self.POINT_RAD = 5                              
        # Bind events to the canvas
        
        canvas.Bind(wx.EVT_MOTION, self.pos_status)
        canvas.Bind(wx.EVT_LEFT_DOWN, self.record_point)
        done_button.Bind(wx.EVT_BUTTON, self.create_matrix)
        
        #Sizers
        canvas.SetMinSize(cal_image.Size)
        image_sizer.Add(canvas, 1, wx.ALL, 5)        
        con_sizer.Add(done_button, 1, wx.ALL, 5)
        con_sizer.Add(self.pos_info, 1, wx.ALL, 5)       
        main_sizer.Add(image_sizer, 1, wx.ALL | wx.GROW, 5)
        main_sizer.Add(con_sizer)
        panel.SetSizer(main_sizer)
        main_sizer.Fit(self)
        self.Layout()
        
        # Show the window, along with initial help
        self.Center()
        diagram.ShowAll(True)
        self.Show(True)
        self.ShowHelp()
        
    def pos_status(self,event):
        '''
            The postion of the mouse will be dynamically given to the user.
        '''
        x,y = event.GetPositionTuple()
        x = str(x)
        y = str(y)
        #y = str(624-y)
        self.pos_info.SetLabel('Pointer at:\n'+x+'\n'+y)
        self.pos_info.Size = (60,60)
        event.Skip()
    
    def ShowHelp(self):
        '''
           Help the user understand about this calibration procedure
        '''
        help_string = '''This window will help you calibrate the virtual space,
        the space where you click points. You will need to click 4 points
        , no more no less. The order is as follows:
            P1.                  .P2
            


            P3.                  .P4'''
            
        wx.MessageBox(help_string, 'Info', 
                  wx.OK | wx.ICON_INFORMATION)
    def home(self):
        processor.write('h')
        suma=processor.read(1)
                  
    def ShowOverflowError(self):
        '''
            The user has clicked more points than necessary, tell him that
        '''     
        help_string = '''You have clicked more points that necessary. 
        Assuming it was a mistake, the program will start calibration'''
        wx.MessageBox(help_string, 'Error',
                  wx.OK | wx.ICON_ERROR)
        self.create_matrix(event=None)
        
    def ShowUnderflowError(self):
        '''
            User needs to click more points
        '''
        help_string = '''You have not completed all points'''
        wx.MessageBox(help_string, 'Error',
                  wx.OK | wx.ICON_ERROR)
        
    def record_point(self, event):
        '''
            Record the points where the user clicks
        '''
        
        x,y = event.GetPositionTuple()
        dc = wx.ClientDC(self.canv)
        self.canv.PrepareDC(dc)
        
        start = ogl.CircleShape(self.POINT_RAD)
        start.SetX(x)
        start.SetY(y)
        self.canv.AddShape(start)
        start.Show(True)
        self.points.append([x,y])
        self.count += 1
        
        if self.count >4:
            self.ShowOverflowError()
            
        self.canv.Redraw(dc)
        self.canv.Update()
        
        event.Skip()        
        
    def create_matrix(self, event):
        '''
            Create the homography matrix
        '''
        global x_a4max,y_a4max
        global H_MATRIX
        if self.count < 4:
            self.ShowUnderflowError()
        else:
            real_space = []
            pts1=[]

            for i in range(0,4,1):
                pts1.append(self.points[i])
                
            pts1 = np.float32(pts1)
            #print >> sys.stderr, str(pts1.size)
            pts2 = np.float32([[0,0],[x_a4max,0],[0,y_a4max],[x_a4max,y_a4max]])
            H_MATRIX = cv2.getPerspectiveTransform(pts1,pts2)
            
            f = open('Data/cam_config','w')
            for line in H_MATRIX:
                for i in line:
                    f.write(str(i)+'\n')
            f.close()
            wx.MessageBox('Done !!', 'Done',
                  wx.OK | wx.ICON_INFORMATION)
            self.Destroy()           


class srCalibrateMech(wx.Frame):
    '''
        This window will help to correct errors caused due to machine, In this we have
        provided fully automatic mode and manual mode for calibration. In automatic mode
        machine itself print and calibrate.
    '''
    boundpt=[]
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, "Calibrate mechanical workspace")
        panel = wx.Panel(self)
        global H_MATRIX
        self.getImage()
        ###############Read h matrix from file###########
        self.bound= open('Data/cam_config')
        
        for i in range(0,3):
            self.xp=float(self.bound.readline())
            self.yp=float(self.bound.readline())
            self.zp=float(self.bound.readline())
            self.boundpt.append([self.xp,self.yp,self.zp])
        self.bound.close()
        self.boundpt=np.array((self.boundpt))
        img = self.im_resized
        cv2.cvtColor(img, cv2.COLOR_BGR2RGB,img)
        dst = cv2.warpPerspective(img,self.boundpt,(x_a4max,y_a4max))
        cv2.imwrite('bin/mech_calibrate.jpg',dst)
        cal_image = wx.Bitmap('bin/mech_calibrate.jpg')
        
        # Create the box sizers to hold the image, info box
        # and the universal sizer
        
        image_sizer = wx.BoxSizer(wx.HORIZONTAL)
        info_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer = wx.BoxSizer(wx.HORIZONTAL)
        control = wx.StaticBox(panel,label = 'Control Area')
        con_sizer = wx.StaticBoxSizer(control,orient = wx.VERTICAL)
        
        # Create OGL system and embedding the image to GUI
        
        canvas = ogl.ShapeCanvas(panel)
        diagram = ogl.Diagram()
        canvas.SetDiagram(diagram)
        diagram.SetCanvas(canvas)
        bg = ogl.BitmapShape()
        bg.SetBitmap(cal_image)
        bg.SetDraggable(False)
        xsize,ysize = cal_image.Size
        bg.SetX(int(xsize/2))
        bg.SetY(int(ysize/2))
        canvas.AddShape(bg)
        self.canv = canvas
        self.pos_info = wx.StaticText(panel,-1,
                                      'Mouse Position')
        # Create a button to help user tell that calibration is done in manual mode
        
        done_button = wx.Button(panel, label = 'Done',size = (60,30))
        self.count = 0
        self.points = []
        self.POINT_RAD = 5
        
        # Bind events to the canvas
        
        canvas.Bind(wx.EVT_MOTION, self.pos_status)
        canvas.Bind(wx.EVT_LEFT_DOWN, self.record_point)
        done_button.Bind(wx.EVT_BUTTON, self.create_matrix)
        
        #Sizers
        
        canvas.SetMinSize(cal_image.Size)
        image_sizer.Add(canvas, 1, wx.ALL, 5)        
        con_sizer.Add(done_button, 1, wx.ALL, 5)
        con_sizer.Add(self.pos_info, 1, wx.ALL, 5)        
        main_sizer.Add(image_sizer, 1, wx.ALL | wx.GROW, 5)
        main_sizer.Add(con_sizer)
        panel.SetSizer(main_sizer)
        main_sizer.Fit(self)
        self.Layout()
        
        # Show the window, along with initial help
        
        self.Center()
        diagram.ShowAll(True)
        self.Show(True)
        self.ShowHelp()
        ###################################Auto mach offset correction#############3
        
        if(self.opt==4):
            self.calpattern()#To print calibration pattern
            self.getImage()
            ###############Read h matrix from file###########
            self.bound= open('Data/cam_config')
            self.boundpt=[]
            for i in range(0,3):
                self.xp=float(self.bound.readline())
                self.yp=float(self.bound.readline())
                self.zp=float(self.bound.readline())
                self.boundpt.append([self.xp,self.yp,self.zp])
            self.bound.close()
            self.boundpt=np.array((self.boundpt))
            img = self.im_resized
            dst = cv2.warpPerspective(img,self.boundpt,(x_a4max,y_a4max))
            cv2.imwrite('bin/mech_calibrate1.jpg',dst)
            
            cal_image = wx.Bitmap('bin/mech_calibrate1.jpg')
            img0=cv2.imread('bin/mech_calibrate1.jpg',0)#printed image
            img0 = cv2.medianBlur(img0,3)

            '''Adaptive thresholding is used to overcome random brightness(shadow) level in captured image,
            Which cause undesired results'''
            thresh= cv2.adaptiveThreshold(img0,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,1)
            #To Invert the White and black pixels
            ret, thresh = cv2.threshold(thresh,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
            #Morphology close filter to is used to clear small black regions in thresholded image
            kernel = np.ones((3,3),np.uint8)
            opening = cv2.morphologyEx(thresh,cv2.MORPH_CLOSE,kernel, iterations =3)
            output=thresh
            output=output*0
            row,col=opening.shape
            cv2.imwrite('bin/thes.bmp',opening)
            cpy = np.zeros((row,col),np.uint8)
            for a in range(0,row-40):
                for b in range(0,col-40):
                    cpy[20+a][20+b]=1
            cv2.imwrite('bin/mul.bmp',cpy)
            #To create intersection detection window
            wind = np.zeros((51,51),np.uint8)
            res=opening
            for a in range(0,51):
                wind[a][25]=255
                wind[25][a]=255
            row1,col1=wind.shape
            #For intersection detection 
            opening = cv2.morphologyEx(opening,cv2.MORPH_OPEN,wind, iterations =1)#To filter the intersection, must be 1 itr
            kernel = np.ones((3,3),np.uint8)
            bkp_img=output
            cv2.imwrite('bin/res2.bmp',output)
            bkp_img=cv2.imread('bin/res2.bmp',0)
            #Remove the boundary pix 
            opening=np.multiply(opening, cpy)
            cv2.imwrite('bin/6w.bmp',opening)
            contours, hierarchy = cv2.findContours(opening, cv2.RETR_EXTERNAL,    
                cv2.CHAIN_APPROX_NONE)#This give approx location
            ret, bkp_img = cv2.threshold(bkp_img,0,255,cv2.THRESH_OTSU)
            aprx_pt=[]
            
            '''The regions detected after intersection filtering need to be validated based on area of each region and count
            in our case it must be 4'''
            
            conn=0
            for cnt in contours:
                area=cv2.contourArea(cnt)
                (xc,yc),radius = cv2.minEnclosingCircle(cnt)
                print >> sys.stderr, str(area)
                if((area>150)and (area<950)):#area check in detected regions
                    output[yc][xc]=255
                    xc=x_a4max-xc
                    aprx_pt.append([xc,yc])
                else:
                    del contours[conn]
                conn+=1               
            cv2.imwrite('bin/out.bmp',output)
                    
            if((len(contours)==4)):#To verify the count of the regions
                dis=[]
                print >> sys.stderr, 'enter'
                #To order detected 4 points
                for a in range(0,4):
                    x1,y1=aprx_pt[a]
                    l=math.sqrt(((x1)*(x1))+((y1)*(y1)))
                    dis.append(l)
                diss=sorted(dis)
                diso=[]
                for a in range(0,len(dis)):
                    for b in range(0,len(dis)):
                        if(diss[a]==dis[b]):
                            print(b)
                            diso.append(aprx_pt[b])
                print >> sys.stderr, str(diso)
                
                ######################home position correction#########################3
                '''The offset near to the origin of printer is nearly zero, If our image reorganization result will not
                give same value as point near to origin [64,64]. So this difference is considered as offset and need
                need to be corrected as shown below.'''
                x1,y1=diso[0]
                hof=[]
                x,y=[64,64]
                x_off=x1-x
                y_off=y1-y
                hof=[x_off,y_off]
                f = open('Data/home_config','w')
                for i in hof:
                        f.write(str(i)+'\n')
                f.close()

                pts1 = np.float32([diso[1],diso[0],diso[3],diso[2]])#auto found point
                pts2 = np.float32([[395,64],[64,64],[395,581],[64,581]])
                M, mask=cv2.findHomography(pts2,pts1)
                f = open('Data/mech_config','w')
                for line in M:
                    for i in line:
                        f.write(str(i)+'\n')
                f.close()
                wx.MessageBox('Done !!', 'Done',
                      wx.OK | wx.ICON_INFORMATION)
                self.Destroy()
            else:
                '''If automatic machine error correction does not worke, below part will help to manualy calibrate with the
                   preprinted calibration tactile sheet.
                '''
                self.caliberror()#This open a window and tell to try manual mode
                
        else:
            print('man mode activated')

    def calpattern(self):#Calibration pattern
        self.home()
        self.init()
        self.tra(119,0)
        self.init()
        #Pattern a1
        center=[128,64]
        x1,y1=center
        self.tra(x1,y1)
        self.extr_press()
        processor.write('a')
        suma=processor.read(1)
        time.sleep(2.8)
        x1=x1-10
        self.tra(x1,y1)
        time.sleep(.8)
        x1=x1-118
        self.tra(x1,y1)
        time.sleep(.8)
        y1=y1-64
        self.tra(x1,y1)
        time.sleep(.8)
        x1=x1+64
        self.tra(x1,y1)
        time.sleep(.6)
        y1=y1+108
        self.tra(x1,y1)
        
        time.sleep(.6)
        y1=y1+20
        self.tra(x1,y1)
        processor.write('a')
        suma=processor.read(1)
        self.extr_nopress()
        time.sleep(2)
        
        #pattern a2
        center=[64,517]
        x1,y1=center
        self.tra(x1,y1)
        self.extr_press()
        processor.write('a')
        suma=processor.read(1)
        time.sleep(2.6)
        y1+=10
        self.tra(x1,y1)
        time.sleep(.8)
        y1+=118
        self.tra(x1,y1)
        time.sleep(.8)
        x1-=64
        self.tra(x1,y1)
        time.sleep(.8)
        y1-=64
        self.tra(x1,y1)
        time.sleep(.6)
        x1+=108
        self.tra(x1,y1)
        
        time.sleep(.6)
        x1+=20
        self.tra(x1,y1)
        processor.write('a')
        suma=processor.read(1)
        self.extr_nopress()
        time.sleep(2)
        
        #Pattern a3
        center=[331,581]
        x1,y1=center
        self.tra(x1,y1)
        self.extr_press()
        processor.write('a')
        suma=processor.read(1)
        time.sleep(2.6)
        x1+=10
        self.tra(x1,y1)
        time.sleep(.8)
        x1+=118
        self.tra(x1,y1)
        time.sleep(.8)
        y1+=64
        print >> sys.stderr, str([x1,y1])
        self.tra(x1,y1)
        time.sleep(.8)
        x1-=64
        print >> sys.stderr, str([x1,y1])
        self.tra(x1,y1)
        time.sleep(.6)
        y1-=108
        print >> sys.stderr, str([x1,y1])
        self.tra(x1,y1)
        time.sleep(.6)
        y1-=20
        self.tra(x1,y1)
        processor.write('a')
        suma=processor.read(1)
        self.extr_nopress()
        time.sleep(2)
        
        #Pattern a4
        center=[395,128]
        x1,y1=center
        self.tra(x1,y1)
        self.extr_press()
        processor.write('a')
        suma=processor.read(1)
        time.sleep(2.6)
        y1-=10
        self.tra(x1,y1)
        time.sleep(.8)
        y1-=118
        self.tra(x1,y1)
        time.sleep(.8)
        x1+=64
        self.tra(x1,y1)
        time.sleep(.8)
        y1+=64
        self.tra(x1,y1)
        time.sleep(.6)
        x1-=108
        self.tra(x1,y1)
        
        time.sleep(.6)
        x1-=20
        self.tra(x1,y1)
        processor.write('a')
        suma=processor.read(1)
        self.extr_nopress()
        time.sleep(2)
        self.home()

    def extr_press(self):
        processor.write('5')
        suma=processor.read(1)
        processor.write('a')
        suma=processor.read(1)
        time.sleep(1.2)
        processor.write('a')
        suma=processor.read(1)
        processor.write('5')
        suma=processor.read(1)
        
    def extr_nopress(self):
        processor.write('5')
        suma=processor.read(1)
        processor.write('q')
        suma=processor.read(1)
        time.sleep(1.2)
        processor.write('q')
        suma=processor.read(1)
        processor.write('5')
        suma=processor.read(1)
    def init(self):
        self.x0=0
        self.y0=0
        self.x1=0
        self.y1=0
    def tra(self,x1,y1):
        #This generate the steps for Tactograph(Bresenhamís line algorithm)
        print >> sys.stderr, str([x1,y1])
        processor.write('r')#This inform tacto for the arriv of new line and reset 
        suma=processor.read(1)
        global move
        dis=math.sqrt((x1-self.x0)*(x1-self.x0)+(y1-self.y0)*(y1-self.y0))
        x1=int(x1)
        y1=int(y1)
        dis=1
        if(dis):
                         
                        if (x1 == self.x0)and (y1!=self.y0):#St line in +-y direction
                                
                                slo_y=(y1-self.y0)
                                if(slo_y<0):
                                        
                                        slo_y=abs(slo_y)
                                        #print(slo_y)
                                        for yi in range(0,slo_y-1):
                                                processor.write('8')
                                                suma=processor.read(1)
                                        processor.write('d')
                                        suma=processor.read(1)
                                        processor.write('8')#last step deramp
                                        suma=processor.read(1)
                                        
                                                
                                                
                                                
                                else:
                                        slo_y=abs(slo_y)
                                        for yi in range(0,slo_y-1):
                                                processor.write('2')
                                                suma=processor.read(1)
                                        processor.write('d')
                                        suma=processor.read(1)
                                        processor.write('2')
                                        suma=processor.read(1)
                                        
                                                
                                        
                                        
                        elif (y1 == self.y0) and (x1!=self.x0):#St line in +-x direction
                                
                            
                                slo_x=(x1-self.x0)
                                if(slo_x>0):
                                        slo_x=abs(slo_x)
                                        for xi in range(0,slo_x-1):
                                                processor.write('4')
                                                suma=processor.read(1)
                                        processor.write('d')
                                        suma=processor.read(1)
                                        processor.write('4')
                                        suma=processor.read(1)
                                else:
                                        slo_x=abs(slo_x)
                                        for xi in range(0,slo_x-1):
                                                processor.write('6')
                                                suma=processor.read(1)
                                                
                                        processor.write('d')
                                        suma=processor.read(1)
                                        processor.write('6')
                                        suma=processor.read(1)
                                
                        elif(y1==self.y0) and (x1==self.x0):
                                print('same point')
                                #nop
                        
                        
                        
                                
                        else:
                                slo_y=(y1-self.y0)
                                slo_x=(x1-self.x0)
                                slo_xa=abs(slo_x)
                                slo_ya=abs(slo_y)
                                print(slo_x,slo_y)
                                slope= slo_y/slo_x
                                slopee=abs(slope)
                                '''Since the four coordinates are simitrical, points are shifted to first coordinate and steps are generated'''
                                if((slo_x>0) and (slo_y>0)):#ist cord
                                        x11=x1
                                        y11=y1
                                        coord=1
                                elif((slo_x<0) and (slo_y>0)):#second cord
                                        x11=self.x0+slo_xa
                                        y11=y1
                                        coord=2
                                elif((slo_x<0) and (slo_y<0)):#3rd cord
                                        x11=self.x0+slo_xa
                                        y11=self.y0+slo_ya
                                        coord=3
                                        

                                else:#4th cord
                                        x11=x1
                                        y11=self.y0+slo_ya
                                        coord=4
                                move=[]                                       
                                if(slopee<=1):
                                        yt=self.y0
                                        for xt in range(self.x0+1,x11+1):
                                                if((abs((slopee*(xt-self.x0))+self.y0-yt))>=(abs((slopee*(xt-self.x0))+self.y0-(yt+1)))):
                                                    yt=yt+1
                                                    move.append('d') 
                                                else:
                                                    move.append('s')
                                                                                      
                                elif(slopee>1):
                                        xt=self.x0
                                        dere=0
                                        for yt in range(self.y0+1,y11+1):

                                                if((abs((slopee*(xt-self.x0))+self.y0-yt))>=(abs((slopee*((xt+1)-self.x0))+self.y0-(yt)))):
                                                    xt=xt+1
                                                   
                                                    move.append('d')
                                                   
                                                else:
                                                    move.append('s')

                                else:
                                        print('')
                                move.append('r')
                                
                                for ste in range(0,len(move)-1):
                                        if(move[ste]!=move[ste+1]):
                                                processor.write('d')
                                                suma=processor.read(1)
                                                
                                        stee=move[ste]
                                        if(stee=='d'):
                                                
                                                
                                                    if(coord==1):
                                                            processor.write('1')
                                                            suma=processor.read(1)
                                                    elif(coord==2):
                                                            processor.write('3')
                                                            suma=processor.read(1)
                                                    elif(coord==3):
                                                            processor.write('9')
                                                            suma=processor.read(1)
                                                    elif(coord==4):
                                                            processor.write('7')
                                                            suma=processor.read(1)
                                                    else:
                                                            print('')
                                        elif(stee=='s'):
                                                
                                                    if(coord==1):
                                                            if(slopee<=1):
                                                                    
                                                                    processor.write('4')
                                                                    suma=processor.read(1)
                                                                    
                                                                
                                                            else:
                                                                    
                                                                    processor.write('2')
                                                                    suma=processor.read(1)
                                                                    
                                                    elif(coord==2):
                                                            if(slopee<=1):
                                                                    
                                                                    processor.write('6')
                                                                    suma=processor.read(1)
                                                                
                                                            else:
                                                                    
                                                                    processor.write('2')
                                                                    suma=processor.read(1)
                                                                    
                                                    elif(coord==3):
                                                            if(slopee<=1):
                                                                    
                                                                    processor.write('6')
                                                                    suma=processor.read(1)
                                                                
                                                            else:
                                                                    
                                                                    processor.write('8')
                                                                    suma=processor.read(1)
                                                                    
                                                    elif(coord==4):
                                                            if(slopee<=1):
                                                                    
                                                                    processor.write('4')
                                                                    suma=processor.read(1)
                                                                
                                                            else:
                                                                    
                                                                    processor.write('8')
                                                                    suma=processor.read(1)
                                                                    print('')
                                                    else:
                                                            print('')
                                        
                                        else:
                                                print('')
                                        


                                
                                
                                
                        self.x0=x1
                        self.y0=y1                                      

            
                
        else:#dis greter then limi
            print('')
    


    def getImage(self):
        '''
            This method is called at the start to take the guide image 2
        '''
        global BaseImage, BaseCam
        cam = lib.webcam(0.85,5,5,40)
        
        self.im = ress.open("bin/color.jpg")
        a,b=self.im.size
        a=int(1.5*a)
        b=int(1.5*b)
        self.size = a, b
        self.im_resized = self.im.resize(self.size)
        self.im_resized = np.array(self.im_resized)
        #self.im_resized.save("bin/web.bmp", "BMP")

    def home(self):
        processor.write('h')
        suma=processor.read(1)
        
    def pos_status(self,event):
        '''
            The postion of the mouse will be dynamically given to the user.
        '''
        x,y = event.GetPositionTuple()
        x = str(x)
        y = str(y)
        self.pos_info.SetLabel('Pointer at:\n'+x+'\n'+y)
        self.pos_info.Size = (60,60)
        event.Skip()
    
    def ShowHelp(self):
        '''
           Help the user understand about this window
        '''
        help_string = '''This window will help you calibrate the mechanical
        workspace, so that any glitches due to measurement can be corrected.
        just keep A4 sheet on platform, refil the tactile fluid and click OK to in start the
        automatic calibration.
        '''
            
        self.opt=wx.MessageBox(help_string, 'Info', 
                  wx.OK|wx.ALL| wx.CANCEL| wx.ICON_INFORMATION )
        #print >> sys.stderr, str(opt)
                  
    def ShowOverflowError(self):
        '''
            The user has clicked more points than necessary, tell him that
        '''     
        help_string = '''You have clicked more points that necessary. 
        Assuming it was a mistake, the program will start calibration'''
        wx.MessageBox(help_string, 'Error',
                  wx.OK | wx.ICON_ERROR)
        self.create_matrix(event=None)
        
    def ShowUnderflowError(self):
        '''
            User needs to click more points
        '''
        help_string = '''You have not completed 20 points'''
        wx.MessageBox(help_string, 'Error',
                  wx.OK | wx.ICON_ERROR)
    def caliberror(self):
        '''
            Lnghting is not proper, you may choose mnl method
        '''
        help_string = ''' Lnghting is not proper, you may choose mnl method'''
        wx.MessageBox(help_string, 'Error',
                  wx.OK | wx.ICON_ERROR)
        
    def record_point(self, event):
        '''
            Record the points where the user clicks
        '''
        
        x,y = event.GetPositionTuple()
        dc = wx.ClientDC(self.canv)
        self.canv.PrepareDC(dc)
        
        start = ogl.CircleShape(self.POINT_RAD)
        start.SetX(x)
        start.SetY(y)
        self.canv.AddShape(start)
        start.Show(True)
        self.points.append([x,y])
        self.count += 1
        
        if self.count > 4:
            self.ShowOverflowError()
            
        self.canv.Redraw(dc)
        self.canv.Update()
        
        event.Skip()        
        
    def create_matrix(self, event):
        '''
            Create the homography matrices
        '''
        
        global HMATRIX
       
        if self.count < 4:
            self.ShowUnderflowError()
        else:
                      
            dis=[]
            for a in range(0,4):
                x1,y1=self.points[a]
                x1=x_a4max-x1
                l=math.sqrt(((x1)*(x1))+((y1)*(y1)))
                dis.append(l)
            print('distance of detected points from orgin\n')
            print(dis)
            print('\n')
            diss=sorted(dis)
            
            print >> sys.stderr,str(dis)
            print >> sys.stderr,str(self.points)
            print >> sys.stderr,str(diss)
            diso=[]
            #sort the points
            for a in range(0,len(dis)):
                for b in range(0,len(dis)):
                    if(diss[a]==dis[b]):
                        print(b)
                        x,y=self.points[b]
                        diso.append([x_a4max-x,y])
                        
            print >> sys.stderr,str(diso)
            pts1 = np.float32([diso[1],diso[0],diso[3],diso[2]])#auto found point
            pts2 = np.float32([[395,64],[64,64],[395,581],[64,581]])
            M, mask=cv2.findHomography(pts2,pts1)
            print >> sys.stderr,str(M)
            print(M)
            f = open('Data/mech_config','w')
            for line in M:
                for i in line:
                    f.write(str(i)+'\n')
            f.close()
            wx.MessageBox('Done !!', 'Done',
                  wx.OK | wx.ICON_INFORMATION)
            self.Destroy()                     

class srMenuBar(wx.MenuBar):
    '''
        This class's is used to create menu bar
    
    '''
    def __init__(self,parent):
        
        locale = wx.Locale(wx.LANGUAGE_ENGLISH)
        
        self.parent = parent        # parent is very important, since, all the
                                    # menu events are bound to parent.
                    
        wx.MenuBar.__init__(self)   # dont forget this line
        # create the menu list first
        file = wx.Menu()            # open, save and quit project
        edit = wx.Menu()            # undo, redo operations
        view = wx.Menu()            # add outline view and solid view option
        tool = wx.Menu()            # add calibrate.
        help = wx.Menu()            # end user and developer help
        about = wx.Menu()           # about Tactograph

        # Next 3 options are for file menu
        open = wx.MenuItem(file, 1, '&Open\tCtrl+O')
        open.SetBitmap(wx.Bitmap('Icons/Folder-Open-icon.png'))
        file.AppendItem(open)
        self.parent.Bind(wx.EVT_MENU, self.onOpen, id = 1)

        save = wx.MenuItem(file, 2, '&Save template\tCtrl+S')
        save.SetBitmap(wx.Bitmap('Icons/Save-as-icon.png'))
        file.AppendItem(save)
        self.parent.Bind(wx.EVT_MENU, self.onSave, id = 2)
        
        quit = wx.MenuItem(file, 3, '&Quit\tAlt+F4')
        quit.SetBitmap(wx.Bitmap('Icons/Close-icon.png'))
        file.AppendItem(quit)
        self.parent.Bind(wx.EVT_MENU,self.onQuit,id = 3)
        
        # Add a caliberate option to tool menu. 
        caliberate = wx.MenuItem(tool, 4, '&Cam Calibrate')
        caliberate.SetBitmap(wx.Bitmap('Icons/camm_.png'))
        tool.AppendItem(caliberate)
        self.parent.Bind(wx.EVT_MENU, self.onCaliberate, id = 4)

        # Add a mechanical calibration option to tool menu. 
        mech_caliberate = wx.MenuItem(tool, 8, '&Mechanical Calibration')
        mech_caliberate.SetBitmap(wx.Bitmap('Icons/machine.png'))
        tool.AppendItem(mech_caliberate)
        self.parent.Bind(wx.EVT_MENU, self.onMechCalibrate, id = 8)

        # Add the manual motor control utility
        man_motor_ctrl = wx.MenuItem(tool, 9, '&Manual motor control')
        man_motor_ctrl.SetBitmap(wx.Bitmap('Icons/motorrr.png'))
        tool.AppendItem(man_motor_ctrl)
        self.parent.Bind(wx.EVT_MENU, self.onMotorCtrl, id = 9)

        # The grid calibrations is an intense one and should be
        # used in case the cam image is very heavily distorted
        grid_calibrate = wx.MenuItem(tool, 5, '&Home position')
        grid_calibrate.SetBitmap(wx.Bitmap('Icons/homee.png'))
        tool.AppendItem(grid_calibrate)
        self.parent.Bind(wx.EVT_MENU, self.onGridCalibrate, id = 5)

        Tac_help = wx.MenuItem(help, 6, '&Tactograph help')
        Tac_help.SetBitmap(wx.Bitmap('Icons/help1_converted.png'))
        help.AppendItem(Tac_help)
        self.parent.Bind(wx.EVT_MENU, self.ontachelp, id = 6)

        # Add view options next
        solid_view = wx.MenuItem(help, 7, '&About Tactograph')
        solid_view.SetBitmap(wx.Bitmap('Icons/solid.png'))
        help.AppendItem(solid_view)
        self.parent.Bind(wx.EVT_MENU, self.onAbout, id = 7)
       
        
        self.Append(file,'&File')
        self.Append(tool, '&Tools')
        self.Append(help, '&Help')
        

    def onQuit(self,event):
        print ' Will now quit'
        Tactograph.OnClose(self.parent,2)
        #self.parent.Close()

    def onOpen(self,event):
       
        if not tflag:
            Tactograph.aplot(self.parent,2)
        else:
            self.plotexc()
       

    def onSave(self,event):
        
        if not tflag:
            Tactograph.savept(self.parent,2)
        else:
            self.plotexc()
            
    def onCaliberate(self, event):
        
        if not tflag:
            srCalibrate(self.parent, 2)
        else:
            self.plotexc()

    def onGridCalibrate(self, event):
        
        if not tflag:
            srCalibrateGrid(self.parent, 2)
        else:
            self.plotexc()

    def onMechCalibrate(self, event):
        
        if not tflag:
            srCalibrateMech(self.parent, 2)
        else:
            self.plotexc()

    def onMotorCtrl(self, event):
        
        if not tflag:
            srManMotorCtrl(self.parent, 2)
        else:
            self.plotexc()
    def onAbout(self,event):
        
        if not tflag:
            a='''Tactograph version 2.0V
            '''
            wx.MessageBox(a, 'Message',
                      wx.OK | wx.ICON_INFORMATION)
        else:
            self.plotexc()

    def ontachelp(self,event):
        
        if not tflag:
            a='''To make the printing process simple, only two keys are used for control
                (Ctrl+mouse click and Shift+mouse click)
                Ctrl+ mouse click, move between point to point without extrusion
                Shift +mouse click, start and end of extrusion(ie.For first time, Shift+click extrusion starts and onsecond time Shift+click exrusion stops). 
                '''
            wx.MessageBox(a, 'Message',
                      wx.OK | wx.ICON_INFORMATION)
        else:
            self.plotexc()
    def plotexc(self):
        print('Printing process in progress,use me after the process over or click stop plot button')

#This class is used to generate steps for motor with Bresenhamís line algorithm

class Joinpoints:
    def __init__(self):
        global shaden,shadev
        self.x0=0
        self.y0=0
        self.x1=0
        self.y1=0
        self.mark=0
        self.length=0
        self.reden1=1
        self.reden0=0
        
    def run(self,pix,ext,boundpt_m,ext_time1,ext_time2):
        global tflag,shaden,dash_space,dash_len,shadev,option
        
        self.pix=pix
        self.ext=ext
        self.boundpt_m=boundpt_m
        
        self.home()
        print >> sys.stderr,str(tflag)
        self.__init__()
        self.tra(113,0)#(52,60)
        self.__init__()
        shift=1
        
        print >> sys.stderr, str(shaden)
        print >> sys.stderr, str(shadev)
        for ii in range(0,len(self.pix)):
            if(tflag==1):
                x,y=self.pix[ii]
                
                    
                
                if (self.ext[ii]=='q'):
                    
                    #this will tog the ext
                    x=x_a4max-x
                    
                    if(shift==1):

                        x,y=self.bulbalg(x,y,ii)
                        x,y=self.error_correction(x,y,self.boundpt_m)
                        self.tra(int(x),int(y))
                        if(shift==1):
                            self.extr_press()
                            processor.write('a')
                            suma=processor.read(1)
                        if(len(shaden)>0):
                            if(shaden.pop(0)==1):#pop(0) mean fifo mode
                                self.mark=1
                                dash_space,dash_len,option=shadev.pop(0)
                                print >> sys.stderr,str([dash_space,dash_len])
                            else:
                                self.mark=0
                        #time.sleep(ext_time1)#This value is read from a file Extr_ config
                        shift=0
                    else:
                        x,y=self.error_correction(x,y,self.boundpt_m)
                        self.tra(int(x),int(y))
                        if(self.reden0==0):
                            
                            processor.write('a')
                            suma=processor.read(1)
                            self.extr_nopress()
                        else:
                            nop=0
                        shift=1
                        if(len(shaden)>0):
                            shaden.pop(0)
                        self.mark=0
                        self.reden1=0
                        self.reden0=0
                        self.length=0
                    
                else:
                    x=x_a4max-x
                    x,y=self.error_correction(x,y,boundpt_m)
                    self.tra(int(x),int(y))
                    #time.sleep(ext_time2)
        if (tflag==1):    
            self.home()
        tflag=0
    def boundary(self,x,y):#To verify all the points are with in boundary after error correction.
        if((x>x_a4max) or (x<0) or (y>y_a4max) or (y<0)):
            global tflag
            if tflag:
                tflag=0
                processor.write('i')
                suma=processor.read(1)
                self.home()
            '''For start printing "d" steps before the start of the line along the slope'''
    def bulbalg(self,x,y,ii):
        global d
        x0,y0=self.pix[ii]
        x1,y1=self.pix[ii+1]
        x1=x_a4max-x1
        x0=x_a4max-x0
        slo_y=(y1-y0)
        slo_x=(x1-x0)
        if ((x1 == x0) and (y1==y0)):#St line in +-y direction
            xd=x0-d
            yd=y0
        elif (x1 == x0)and (y1!=y0):
            if((slo_y)>0):
                xd=x0
                yd=y0-d
            else:
                xd=x0
                yd=y0+d
        elif (x1 != x0)and (y1==y0):
            if(slo_x>0):
               
                xd=x0-d
                yd=y0
            else:
                xd=x0+d
                yd=y0

        else:
            
            slo_y=(y1-y0)
            slo_x=(x1-x0)
            slo_xa=abs(slo_x)
            slo_ya=abs(slo_y)
            slope= slo_y/slo_x
            slopee=abs(slope)
            
            if((slo_x>0) and (slo_y>0)):#ist cord
                    th=self.finddeg(x0,y0,x1,y1)
                    xd=x0+((d)*math.cos(math.radians(th)))
                    yd=y0+((d)*math.sin(math.radians(th)))
                    
                    xd=2*x0-xd
                    yd=2*y0-yd
                    
                    coord=1
            elif((slo_x<0) and (slo_y>0)):#second cord
                    print >> sys.stderr, str(4)
                    th=self.finddeg(x0,y0,x1,y1)
                    xd=x0+((d)*math.cos(math.radians(abs(th))))
                    yd=y0+((d)*math.sin(math.radians(abs(th))))
                    yd=2*y0-yd
                    coord=2
            elif((slo_x<0) and (slo_y<0)):#3rd cord
                    th=self.finddeg(x0,y0,x1,y1)
                    xd=x0+((d)*math.cos(math.radians(th)))
                    yd=y0+((d)*math.sin(math.radians(th)))
                    coord=3
            else:#4th cord
                    print >> sys.stderr, str(6)
                    #x11=x1
                    #y11=y0+slo_ya
                    th=self.finddeg(x0,y0,x1,y1)
                    xd=x0+((d)*math.cos(math.radians(abs(th))))
                    yd=y0+((d)*math.sin(math.radians(abs(th))))
                    xd=2*x0-xd
                    coord=4
        return([xd,yd])
    def finddeg(self,x0,y0,x11,y11):
        return math.degrees(math.atan((y11-y0)/(x11-x0)))
        
    def error_correction(self,x,y,boundpt_m):
        data=np.matrix([[x],[y],[1]])
        cor=np.dot(self.boundpt_m,data)
        xp,yp=[cor[0]/cor[2],cor[1]/cor[2]]
        dx,dy=[xp-x,yp-y]
        xc,yc=[((2*x)-xp),((2*y)-yp)]
        return([round(xc),round(yc)])
    def extr_press(self):
        processor.write('5')
        suma=processor.read(1)
        processor.write('a')
        suma=processor.read(1)
        time.sleep(1.2)
        processor.write('a')
        suma=processor.read(1)
        processor.write('5')
        suma=processor.read(1)
        
    def extr_nopress(self):
        processor.write('5')
        suma=processor.read(1)
        processor.write('q')
        suma=processor.read(1)
        time.sleep(1.2)
        processor.write('q')
        suma=processor.read(1)
        processor.write('5')
        suma=processor.read(1)
        time.sleep(2)
    def home(self):
        processor.write('h')
        suma=processor.read(1)
    def shade(self):
        global dash_space,dash_len,option
        
        if(option==1):
            
            if(self.length>dash_space and self.length<(dash_len+dash_space)):
                
                if (self.reden0==0):
                    processor.write('a')#stop ext
                    suma=processor.read(1)
                    self.extr_nopress()
                    time.sleep(.3)
                    
                    self.reden0=1
                    self.reden1=0
                else:
                    pass
                    
            elif(self.length<=dash_space):
                
                if(self.reden1==0):
                    processor.write('n')
                    suma=processor.read(1)
                    self.reden1=1
                    self.reden0=0
                else:
                    pass
                
            else:
                self.length=0
                self.extr_press()
                processor.write('a')
                suma=processor.read(1)
                time.sleep(.3)
                self.reden0=0               
                
        elif(option==2):#any shade can be added
            pass
        else:
            print('adsfe')
        

        
    def tra(self,x1,y1):
        processor.write('r')#This inform tacto for the arriv of new line and reset 
        suma=processor.read(1)
        global move,tflag
        dis=math.sqrt((x1-self.x0)*(x1-self.x0)+(y1-self.y0)*(y1-self.y0))
        x1=int(x1)
        y1=int(y1)
        dis=1
        

        if (x1 == self.x0)and (y1!=self.y0):#St line in +-y direction
                
                slo_y=(y1-self.y0)
                
                if(slo_y<0):
                        
                        slo_y=abs(slo_y)
                        #print(slo_y)
                        for yi in range(0,slo_y-1):
                            if(tflag==1):
                                    if(self.mark==1):
                                        self.length+=0.31
                                        self.shade()
                                    else:                                                                                                  
                                        self.length=0
                                    processor.write('8')
                                    suma=processor.read(1)
                        if(tflag==1):
                            processor.write('d')
                            suma=processor.read(1)
                            processor.write('8')
                            suma=processor.read(1)
                                
                else:
                        slo_y=abs(slo_y)
                        for yi in range(0,slo_y-1):
                            if(tflag==1):
                                    if(self.mark==1):
                                        self.length+=0.31
                                        self.shade()
                                    else:                                                                                                  
                                        self.length=0
                                    processor.write('2')
                                    suma=processor.read(1)
                        if(tflag==1):
                            processor.write('d')
                            suma=processor.read(1)
                            processor.write('2')
                            suma=processor.read(1)
                        
                                
                        
                        
        elif (y1 == self.y0) and (x1!=self.x0):#St line in +-x direction
                
            
                slo_x=(x1-self.x0)
                if(slo_x>0):
                        slo_x=abs(slo_x)
                        for xi in range(0,slo_x-1):#since st line distance will be constant
                            if(tflag==1):
                                if(self.mark==1):
                                    self.length+=0.31
                                    self.shade()
                                else:                                                                                                  
                                    self.length=0
                            
                                processor.write('4')
                                suma=processor.read(1)
                        if(tflag==1):
                            processor.write('d')
                            suma=processor.read(1)
                            processor.write('4')
                            suma=processor.read(1)
                else:
                        slo_x=abs(slo_x)
                        for xi in range(0,slo_x-1):#since st line distance will be constant
                            if(tflag==1):
                                if(self.mark==1):
                                    self.length+=0.31
                                    self.shade()
                                else:                                                                                                  
                                    self.length=0
                                processor.write('6')
                                suma=processor.read(1)
                        if(tflag==1):
                                
                            processor.write('d')
                            suma=processor.read(1)
                            processor.write('6')
                            suma=processor.read(1)
                
        elif(y1==self.y0) and (x1==self.x0):
                print('same point')
                #nop
        
        
        
                
        else:
                slo_y=(y1-self.y0)
                slo_x=(x1-self.x0)
                slo_xa=abs(slo_x)
                slo_ya=abs(slo_y)
                print(slo_x,slo_y)
                slope= slo_y/slo_x
                slopee=abs(slope)
                if((slo_x>0) and (slo_y>0)):#ist cord
                        
                        x11=x1
                        y11=y1
                        coord=1
                elif((slo_x<0) and (slo_y>0)):#second cord
                        x11=self.x0+slo_xa
                        y11=y1
                        coord=2
                elif((slo_x<0) and (slo_y<0)):#3rd cord
                        x11=self.x0+slo_xa
                        y11=self.y0+slo_ya
                        coord=3
                        

                else:#4th cord
                        x11=x1
                        y11=self.y0+slo_ya
                        coord=4
                move=[]                                       

                if(slopee<=1):
                        yt=self.y0
                      
                        for xt in range(self.x0+1,x11+1):
                         
                                if((abs((slopee*(xt-self.x0))+self.y0-yt))>=(abs((slopee*(xt-self.x0))+self.y0-(yt+1)))):
                                    yt=yt+1
                                    move.append('d')
                                   
                                else:

                                    move.append('s')
                            
                elif(slopee>1):
                        xt=self.x0
                        dere=0
                        for yt in range(self.y0+1,y11+1):
                                if((abs((slopee*(xt-self.x0))+self.y0-yt))>=(abs((slopee*((xt+1)-self.x0))+self.y0-(yt)))):
                                    xt=xt+1
                                   
                                    move.append('d')
                                   
                                else:
                                    move.append('s')
                                

                else:
                        pass
                move.append('r')#this ill be the last data
                for ste in range(0,len(move)-1):
                    if(tflag==1):
                        if(move[ste]!=move[ste+1]):
                                processor.write('d')
                                suma=processor.read(1)
                                
                        stee=move[ste]
                        if(stee=='d'):
                                
                                
                                    if(coord==1):
                                                                                               
                                            if(self.mark==1):
                                                self.length+=0.438
                                                self.shade()
                                            else:                                                                                                  
                                                self.length=0
                                            processor.write('1')
                                            suma=processor.read(1)
                                    elif(coord==2):
                                            if(self.mark==1):
                                                self.length+=0.438
                                                self.shade()
                                            else:                                                                                                  
                                                self.length=0
                                            processor.write('3')
                                            suma=processor.read(1)
                                    elif(coord==3):
                                            if(self.mark==1):
                                                self.length+=0.438
                                                self.shade()
                                            else:                                                                                                  
                                                self.length=0
                                            processor.write('9')
                                            suma=processor.read(1)
                                    elif(coord==4):
                                            if(self.mark==1):
                                                self.length+=0.438
                                                self.shade()
                                            else:                                                                                                  
                                                self.length=0
                                            processor.write('7')
                                            suma=processor.read(1)
                                    else:
                                            pass
                        elif(stee=='s'):
                                
                                    if(coord==1):
                                            if(slopee<=1):
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('4')
                                                    suma=processor.read(1)
                                                    
                                                
                                            else:
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('2')
                                                    suma=processor.read(1)
                                                    
                                    elif(coord==2):
                                            if(slopee<=1):
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('6')
                                                    suma=processor.read(1)
                                                
                                            else:
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('2')
                                                    suma=processor.read(1)
                                                    
                                    elif(coord==3):
                                            if(slopee<=1):
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('6')
                                                    suma=processor.read(1)
                                                
                                            else:
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('8')
                                                    suma=processor.read(1)
                                                    
                                    elif(coord==4):
                                            if(slopee<=1):
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('4')
                                                    suma=processor.read(1)
                                                
                                            else:
                                                    if(self.mark==1):
                                                        self.length+=0.31
                                                        self.shade()
                                                    else:                                                                                                  
                                                        self.length=0
                                                    
                                                    processor.write('8')
                                                    suma=processor.read(1)
                                                    
                                    else:
                                            pass
                        
                        else:
                                pass
                
        self.x0=x1
        self.y0=y1
  
class PopupMenu(wx.Menu):#Right click option menu
    
    def __init__(self, parent):
        super(PopupMenu, self).__init__()
        
        self.parent = parent
        
        ne = wx.MenuItem(self, wx.NewId(), 'New')
        self.AppendItem(ne)
        self.Bind(wx.EVT_MENU, self.Onnew, ne)

        plt = wx.MenuItem(self, wx.NewId(), 'Plot')
        self.AppendItem(plt)
        self.Bind(wx.EVT_MENU, self.onplot, plt)

        dot0 = wx.MenuItem(self, wx.NewId(), 'dot shade 6 mm')#
        self.AppendItem(dot0)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,6,2), dot0)

        dot1 = wx.MenuItem(self, wx.NewId(), 'dot shade 8mm')#
        self.AppendItem(dot1)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,8,2), dot1)

        dot2 = wx.MenuItem(self, wx.NewId(), 'dot shade 10 mm')#
        self.AppendItem(dot2)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,10,2), dot2)

        dash = wx.MenuItem(self, wx.NewId(), 'dash shade 4 mm - 4 mm')
        self.AppendItem(dash)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,4,4), dash)

        dash1 = wx.MenuItem(self, wx.NewId(), 'dash shade 4 mm - 6mm')
        self.AppendItem(dash1)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,4,6), dash1)

        dash2 = wx.MenuItem(self, wx.NewId(), 'dash shade 4 mm - 8mm')
        self.AppendItem(dash2)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,4,8), dash2)

        dash3 = wx.MenuItem(self, wx.NewId(), 'dash shade 6 mm - 4mm')
        self.AppendItem(dash3)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,6,4), dash3)

        dash4 = wx.MenuItem(self, wx.NewId(), 'dash shade 6 mm - 6mm')
        self.AppendItem(dash4)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,6,6), dash4)

        dash5 = wx.MenuItem(self, wx.NewId(), 'dash shade 6 mm - 8mm')
        self.AppendItem(dash5)
        self.Bind(wx.EVT_MENU, lambda event:self.dash_shade(event,6,8), dash5)

        cus = wx.MenuItem(self, wx.NewId(), 'custom shade')
        self.AppendItem(cus)
        self.Bind(wx.EVT_MENU, self.custom_shade, cus)
        
    def Onnew(self, event):
        Tactograph.new_img(self.parent,2)

    def onplot(self, event):
        Tactograph.plot(self.parent,2)
       
    def dash_shade(self,event,a,b):
        global dash_space,dash_len,shaden,shadev,shade_e,option
        option=1
        
        if not tflag:
            if(shade_e==1):#meen the last entred option is not executed,because shade_e=1 made hear only
                shadev.pop()
                shadev.append([b,a,option])
                shade_e=1
                
            else:
                shadev.append([b,a,option])
                
                shade_e=1
        else:
            self.plotexc()

    def custom_shade(self,event):
        if not tflag:
            customshade(self.parent, 2)
        else:
            self.plotexc()

class customshade(wx.Frame):
    '''
        This is to get the custon shade space and length value in a GUI
    '''
    
    def __init__(self,parent,id):
        wx.Frame.__init__(self,parent,id,'Custom shade wizard',
                          style = wx.CLOSE_BOX |
                          wx.SYSTEM_MENU | wx.CAPTION)
        panel = wx.Panel(self)
        self.len=5#default
        self.spa=5

        space =[str(x) for x in range(5, 15)]#To create list of options
        length=[str(x) for x in range(5, 15)]
        com_spa = wx.ComboBox(panel, choices=space, 
            style=wx.CB_READONLY)
        com_len = wx.ComboBox(panel,choices=length, 
            style=wx.CB_READONLY)
        ok_but = wx.Button(panel,label = 'Ok',size = (50,30))
        com_spa.Bind(wx.EVT_COMBOBOX, self.space_shade)
        com_len.Bind(wx.EVT_COMBOBOX, self.len_shade)
        ok_but.Bind(wx.EVT_BUTTON,self.ok)
        self.mBox1 = wx.StaticBox(panel, label = 'Shade space')
        self.mBox2 = wx.StaticBox(panel, label = 'Shade length')
        st1 = wx.StaticText(panel, label='mm')
        st2 = wx.StaticText(panel, label='mm')
        sizer1 = wx.StaticBoxSizer(self.mBox1,orient = wx.HORIZONTAL)
        sizer2 = wx.StaticBoxSizer(self.mBox2,orient = wx.HORIZONTAL)
        sizer4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer = wx.BoxSizer(wx.HORIZONTAL)     
        # Add stuff to sizers.
        sizer1.Add(com_spa,1,wx.ALL,5)
        sizer1.Add(st1,1,wx.ALIGN_CENTER_VERTICAL)
        sizer2.Add(com_len,1,wx.ALL,5)
        sizer2.Add(st2,1,wx.ALIGN_CENTER_VERTICAL)
        sizer.Add(sizer1,1,wx.ALL,5)
        sizer.Add(sizer2,1,wx.ALL,5)
        sizer.Add(ok_but,1,wx.EXPAND | wx.ALL |wx.ALIGN_CENTER_VERTICAL,20)
        panel.SetSizer(sizer)
        sizer.Fit(self)
        self.Centre()
        self.Show(True)
    def space_shade(self, event):
        
        self.spa = int(event.GetString())

    def len_shade(self, event):
        
        self.len= int(event.GetString())

    def ok(self,event):
        global shadev,shade_e,option
        option=1
        if not tflag:
            if(shade_e==1):#To check do the  last entred option is not executed,because shade_e=1 made hear only
                shadev.pop()
                shadev.append([self.len,self.spa,option])
                shade_e=1
                
            else:
                shadev.append([self.len,self.spa,option])
                
                shade_e=1
        else:
            self.plotexc()
        self.Destroy()
    def cancel(self,event):
        self.Destroy()
        
class Redirect:
    def __init__(self,sb):
        self.status = sb
    def write(self,line):
        line = line.strip()
        if len(line) > 0:
            self.status.SetStatusText(line)

class SplashScreen(wx.SplashScreen):
    '''
        The splash screen make the user to indicate program loading 
    '''
    def __init__(self,parent):
        
        im = wx.Image("Icons/splash.gif").ConvertToBitmap()        
        style = wx.SPLASH_NO_TIMEOUT | wx.SPLASH_CENTRE_ON_SCREEN | wx.NO_BORDER
        wx.SplashScreen.__init__(self,im,style,-1,parent)
        self.Show(True)

class Tactograph(wx.Frame):
    '''
        This is main GUI class. This window will show up after the splash screen
        is killed and all the loading is over.
    '''
    global H_MATRIX
    boundpt=[]
    boundpt_m=[]
    point1=[]
    point2=[]
    line1=[]
    def __init__(self,parent,id):

        self.parent = parent  
        self.sta=0#sta variable def clicked point is segment or point
        wx.Frame.__init__(self,parent,id,'Tactograph')
        favicon = wx.Icon('Icons/iitm.ico', wx.BITMAP_TYPE_ICO, 32,32)
        wx.Frame.SetIcon(self, favicon)
        splash = SplashScreen(None) # show the splash screen
        # Make sure device is connected
        self.home() 
        device_not_found = False
        if motor.dev is None:
            wx.MessageBox('Device not found. \
                          Please check the connect and retry.', 'Error',
                          wx.OK | wx.ICON_ERROR)
            device_not_found = True
        wx.CallLater(1000, splash.Destroy)                
        panel = wx.Panel(self)
        self.panel=panel
        self.Bind(wx.EVT_CLOSE, self.OnClose)

        
        ################Read h matrix from file for mech########
        self.bound= open('Data/mech_config')
        for i in range(0,3):
            self.xp=float(self.bound.readline())
            self.yp=float(self.bound.readline())
            self.zp=float(self.bound.readline())
            self.boundpt_m.append([self.xp,self.yp,self.zp])
        self.bound.close()
        self.boundpt_m=np.array((self.boundpt_m))
        
        ##################home confit#################
        self.bound= open('Data/home_config')
        
        for i in range(0,1):
            self.xh=float(self.bound.readline())
            self.yh=float(self.bound.readline())
        print >> sys.stderr, str([self.xh,self.yh])
        ##################Extrusion rate file read#########################
        self.bound= open('Data/Extr_config')
        for i in range(0,1):
            self.ext_time1=float(self.bound.readline())
            self.ext_time2=float(self.bound.readline())
            
        print >> sys.stderr, str([self.ext_time1,self.ext_time2])
        # Take a snap of the image from the webcam,
        self.getImage()
        
        ###############Read h matrix from file for camara####################
        self.bound= open('Data/cam_config')
        
        for i in range(0,3):
            self.xp=float(self.bound.readline())
            self.yp=float(self.bound.readline())
            self.zp=float(self.bound.readline())
            self.boundpt.append([self.xp,self.yp,self.zp])
        self.bound.close()
        self.boundpt=np.array((self.boundpt))
        img = self.im_resized
        cv2.cvtColor(img, cv2.COLOR_BGR2RGB,img)
        dst = cv2.warpPerspective(img,self.boundpt,(x_a4max,y_a4max))
        global guide
        
        cv2.imwrite('bin/snap.jpg',dst)
        lib.Image.fromarray(dst).convert('L').save('bin/snapr.bmp')
        guide = wx.Bitmap('bin/snap.jpg')
        
        

        ###################The box sizers will be intiated here################
        sizer = wx.BoxSizer(wx.HORIZONTAL)        # main sizer
        canvas_sizer = wx.BoxSizer(wx.VERTICAL)   # sizer for the canvas
        
        sub_sizer = wx.BoxSizer(wx.VERTICAL)      # Sizer for holding buttons
        control = wx.StaticBox(panel,label = 'Control Buttons')
        pos_stat = wx.StaticBox(panel,label = 'Mouse position status')

        ####Create instance of srMenuBar to create the complete menu system####
        menubar = srMenuBar(self)
        self.SetMenuBar(menubar)       
        con_sizer = wx.StaticBoxSizer(control,orient = wx.VERTICAL)
        
        ###########the control buttons should go into this sizer###############
        pos_stat_sizer = wx.StaticBoxSizer(pos_stat,orient = wx.wx.VERTICAL)

        # All the buttons will be created next
        new_btn = wx.Button(panel, label =  '    New   ',size = (70,30))
        plot_btn = wx.Button(panel, label =  '    Plot   ',size = (70,30))
        plots_btn = wx.Button(panel, label =  'Stop plot',size = (70,30))
        aplot_btn = wx.Button(panel, label = 'Load points',size = (70,30))
        savept_btn = wx.Button(panel, label = 'Save Points',size = (70,30))
        self.mouse_pos = wx.StaticText(panel,size = (60,60))
        self.click_pos = wx.StaticText(panel,size = (120,80),style = wx.BORDER_SUNKEN)
        self.mouse_pos.SetMinSize((60,60))
        self.click_pos.SetMinSize((120,80))
        con_sizer.Add(new_btn,1,wx.ALL,5)
        con_sizer.Add(plot_btn,1,wx.ALL,5)
        con_sizer.Add(plots_btn,1,wx.ALL,5)
        con_sizer.Add(aplot_btn,1,wx.ALL,5)
        con_sizer.Add(savept_btn,1,wx.ALL,5)
        pos_stat_sizer.Add(self.mouse_pos,1,wx.ALL | wx.GROW,5)
        pos_stat_sizer.Add(self.click_pos,1,wx.ALL | wx.GROW,5)
        self.segments = [[]]   # this will hold all the circle shapes
        self.segment_list =[[]]# this will hold all the coordinates in pixels
        self.anchors = [[]]    # data ready to be sent to plot utility
        self.ext=[]
        self.pix=[]
        global temp
        temp = self.segment_list

        # Create the OGL canvas and the diagram objects.
        # the snap from the webcam will form the background
        self.panel=panel
        canvas = ogl.ShapeCanvas(panel)
        diagram = ogl.Diagram()
        self.diagram=diagram
        canvas.SetDiagram(diagram)
        diagram.SetCanvas(canvas)
        self.bg = ogl.BitmapShape()
        self.bg.SetBitmap(guide)
        self.bg.SetDraggable(False)
        xsize,ysize = guide.Size
        self.bg.SetX(int((x_a4max)/2))
        self.bg.SetY(int((y_a4max)/2))
        canvas.AddShape(self.bg)
        self.canv = canvas
        canvas.SetMinSize(guide.Size)   
        
        ##################Create the status bar#########################################
        self.status = self.CreateStatusBar()
        self.dc = wx.ClientDC(self.canv)
        self.canv.PrepareDC(self.dc)

        ######################All the mouse events are bound here#####################
        
        canvas.Bind(wx.EVT_MOTION,self.pos_status)
        canvas.Bind(wx.EVT_LEFT_DOWN,self.newPoint)
        canvas.Bind(wx.EVT_LEFT_DOWN,self.newSegment)
        new_btn.Bind(wx.EVT_BUTTON,self.new_img)
        plot_btn.Bind(wx.EVT_BUTTON,self.plot)
        plots_btn.Bind(wx.EVT_BUTTON,self.plots)
        aplot_btn.Bind(wx.EVT_BUTTON,self.aplot)
        savept_btn.Bind(wx.EVT_BUTTON,self.savept)
        canvas.Bind(wx.EVT_RIGHT_DOWN, self.OnRightDown)
        canvas_sizer.Add(canvas,1,wx.GROW | wx.ALL,10)
        sizer.Add(canvas_sizer,1,wx.GROW)
        sub_sizer.Add(con_sizer)
        sub_sizer.Add(pos_stat_sizer)
        sizer.Add(sub_sizer)
        panel.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()
        self.Centre()
        diagram.ShowAll(True)
        splash.Destroy()    # destroy the splash screen and transfer the
                            # control to main gui
        redirect = Redirect(self.status)
        sys.stdout = redirect
        self.Show(True)

    def OnRightDown(self, event):#Right click option
        self.PopupMenu(PopupMenu(self), event.GetPosition())

    def new_img(self,event):
        '''
            This method is used to reconfigure the GUI for multiple prints
        '''
        global tflag
        if not tflag:
            self.boundpt=[]
            point1=[]
            point2=[]
            line1=[]
            shaden=[]
            shadev=[]
            self.getImage()#Its very important
            ###############Read h matrix from file for camara###########
            self.bound= open('Data/cam_config')
            
            for i in range(0,3):
                self.xp=float(self.bound.readline())
                self.yp=float(self.bound.readline())
                self.zp=float(self.bound.readline())
                self.boundpt.append([self.xp,self.yp,self.zp])
            self.bound.close()
            self.boundpt=np.array((self.boundpt))
            #img = cv2.imread('bin/snap.bmp')
            img = self.im_resized
            cv2.cvtColor(img, cv2.COLOR_BGR2RGB,img)
            dst = cv2.warpPerspective(img,self.boundpt,(x_a4max,y_a4max))
            lib.Image.fromarray(dst).convert('L').save('bin/snapr.bmp')
            global guide
            cv2.imwrite('bin/snap.jpg',dst)
            guide = wx.Bitmap('bin/snap.jpg')
            self.segments = [[]]   # this will hold all the circle shapes
            self.segment_list =[[]]# this will hold all the coordinates in pixels
            self.anchors = [[]]    # data ready to be sent to plot utility
            self.ext=[]
            self.pix=[]       
            self.bg.SetBitmap(guide)
            self.bg.SetDraggable(False)
            xsize,ysize = guide.Size
            self.bg.SetX(int((x_a4max)/2))
            self.bg.SetY(int((y_a4max)/2))
            self.canv.AddShape(self.bg)
            self.canv.SetMinSize(guide.Size)   # Very important step. Else,
            for k in range(0,len(self.point1)):
                self.canv.RemoveShape(self.point1[k])
            for k in range(0,len(self.point2)):
                self.canv.RemoveShape(self.point2[k])
            for k in range(0,len(self.line1)):
                self.canv.RemoveShape(self.line1[k])
            self.canv.Redraw(self.dc)
            self.canv.Update()
        else:
            self.plotexc()
            
    def plotexc(self):
            print('Printing process in progress,use me after the process over or click stop plot button')
        

        
    def pos_status(self,event):
        '''
            The postion of the mouse will be dynamically given to the user.
        '''
        global guide
        x,y = event.GetPositionTuple()
        x = str(x)
        y = str(y)
        self.mouse_pos.SetLabel('Pointer at:\n'+str(x_a4max-int(x))+'\n'+y)
        self.mouse_pos.Size = (60,60)
        event.Skip()
    def plots(self,event):
        global tflag
        if tflag:
            tflag=0
            processor.write('i')
            suma=processor.read(1)
            self.home()
    def OnClose(self,event):
        global tflag
        tflag=0
        processor.write('i')
        processor.write('h')        
        self.Destroy()

    def newPoint(self,event):
        '''
            This method will help the user to add a new point to the
            existing segment
        '''
        #global guide
        if event.ControlDown():
           x,y = event.GetPositionTuple()
           text = 'New point\ncreated at:\nx = #\ny= @'
           self.click_pos.SetLabel(text.replace('#',str(x)).replace('@',str(y)))
           self.click_pos.Size = (120,80)
           self.dc = wx.ClientDC(self.canv)
           self.canv.PrepareDC(self.dc)
           newpt = ogl.CircleShape(POINT_RAD)
           newpt.SetX(x)
           newpt.SetY(y)
           self.canv.AddShape(newpt)
           self.newpt=newpt
           cline = ogl.LineShape()
           cline.MakeLineControlPoints(2)
           cline.Initialise()
           cline.SetPen(wx.Pen('red',1))
           newpt.AddLine(cline,self.segments[-1][-1])
           self.segments[-1].append(newpt)
           self.segment_list[-1].append([x,y])
           self.canv.AddShape(cline)
           self.point1.append(newpt)
           self.line1.append(cline)
           self.cline=cline
           self.ext.append('0')
           self.pix.append([x,y])
           newpt.Show(True)
           cline.Show(True)
           self.canv.Redraw(self.dc)
           self.canv.Update()
           
           event.Skip()
        event.Skip()

    def newSegment(self,event):
        '''
            This method helps to create a new segment on the canvas
        '''
        global shaden,shade_e
        if event.ShiftDown():
            
            x,y = event.GetPositionTuple()
            xsize,ysize = guide.Size
            text = 'New segment\nstarted at:\nx = #\ny= @'
            self.click_pos.SetLabel(text.replace('#',str(x_a4max-x)).replace('@',str(y)))
            self.click_pos.Size = (120,80)
            self.dc = wx.ClientDC(self.canv)
            self.canv.PrepareDC(self.dc)
            start = ogl.CircleShape(NEW_POINT_RAD)
            start.SetX(x)
            start.SetY(y)
            self.canv.AddShape(start)
            self.point2.append(start)
            self.start=start           
            start.Show(True)
            self.segments.append([])
            self.segment_list.append([])
            self.segments[-1].append(start)
            self.segment_list[-1].append([x,y])
            self.ext.append('q')
            self.pix.append([x,y])
            print >> sys.stderr, 'enter0'
            if(shade_e==1):
                print >> sys.stderr, 'enter1'
                shaden.append(1)
                shade_e=0
            else:
                shaden.append(0)
                
            
            self.canv.Redraw(self.dc)
            self.canv.Update()
            event.Skip()
            self.sta=1
        event.Skip()


    
        
    def getImage(self):
        '''
            This method is called at the start to take the guide image 1
        '''
    
        global BaseImage, BaseCam
        cam = lib.webcam(0.85,5,5,30)
        self.im = ress.open("bin/color.jpg")
        a,b=self.im.size
        print >> sys.stderr, str(([a,b]))
        a=int(1.5*a)
        b=int(1.5*b)
        self.size = a, b
        self.im_resized = self.im.resize(self.size)
        self.im_resized = np.array(self.im_resized) 

    def getPoints(self):
        '''
            This method will retrieve the list of all the anchor points
            on the canvas
        '''
        plist = [[]]
        for segment in self.segments:
            for point in segment:
                x = point.GetX() / 20.0
                y = point.GetY() / 20.0
                plist[-1].append([x,y])
            plist.append([])
        return plist

    def error_correction(self,x,y):
        data=np.matrix([[x],[y],[1]])
        cor=np.dot(self.boundpt_m,data)
        xp,yp=[cor[0]/cor[2],cor[1]/cor[2]]
        dx,dy=[xp-x,yp-y]
        xc,yc=[((2*x)-xp),((2*y)-yp)]
        return([round(xc),round(yc)])

    def plot(self,event):#This method is used for plot function is run in a different thread for avoiding the GUI freeze
        '''
            The plot routine. This method will take care of moving the
            Tantograph to the points clicked by the user
            
        '''
        global tflag
        if not tflag:        
            tflag=1
            var=''
            plotT=threading.Thread(target=joinpoints.run,args=(self.pix,self.ext,self.boundpt_m,self.ext_time1,self.ext_time2))
            self.plotT=plotT
            self.plotT.start()
        else:
            self.plotexc()

    def phase_correlation(self,a, G_b):
        G_a = np.fft.fft2(a)
        fshift = np.fft.fftshift(G_a)
        rows, cols = a.shape
        crow,ccol = rows/2 , cols/2
        G_a[crow-30:crow+30, ccol-30:ccol+30] = 0
        conj_b = np.ma.conjugate(G_b)
        R = G_a*conj_b
        R /= np.absolute(R)
        r = np.fft.ifft2(R).real
        r=np.abs(r)
        return ([r,G_a])
    def thre(self,rf,cnt):
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(rf)
        return([min_val, max_val,max_loc])

    def aplot(self,event):
        '''
            Automtic plot routine. 
        '''
        
        if not tflag:
            wcd = 'Tactograph template file|*.txt'
            pwd = os.getcwd()
            open_dlg = wx.FileDialog(self.parent, message = 'load template',
                                     defaultDir = pwd,
                                     wildcard = wcd,
                                     style = wx.OPEN| wx.CHANGE_DIR)
            self.ext=[]
            self.pix=[]
            pon=[]
            if open_dlg.ShowModal() == wx.ID_OK:
                path = open_dlg.GetPath()
                path=path[:-4]
                impath=path+'.bmp'
                txtpath=path+'.txt'
                angle=np.genfromtxt(txtpath,comments='#', delimiter=',', skiprows=0, skip_header=0, skip_footer=0,invalid_raise=True)
                a=0
                ang=-20
                im1=cv2.imread('bin/snapr.bmp',0)
                im1 = cv2.medianBlur(im1,5)
                im1= cv2.adaptiveThreshold(im1,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,1)                
                imw1=(cv2.imread(impath,0))
                imw1 = cv2.medianBlur(imw1,5)
                imw1= cv2.adaptiveThreshold(imw1,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,1)
                rows,cols = im1.shape
                max_v=[]
                loc=[]
                angg=[]
                t1=time.clock()
                h, w = imw1.shape[:2]
                im3 = np.zeros_like(im1)
                im3[:h,:w] = imw1[:,:]
                G_b = np.fft.fft2(im3)
                fshift = np.fft.fftshift(G_b)
                rows, cols = im1.shape
                crow,ccol = rows/2 , cols/2
                G_b[crow-30:crow+30, ccol-30:ccol+30] = 0
                imt=im1
                for aa in range(-10,10):
                    M = cv2.getRotationMatrix2D((cols/2,rows/2),aa,1)
                    im1= cv2.warpAffine(imt,M,(cols,rows))
                    rf,G=self.phase_correlation(im1,G_b)
                    rf=rf/np.linalg.norm(rf)
                    a,b,lo=self.thre(rf,1)
                    t2=time.clock()
                    max_v.append(b)
                    loc.append(lo)
                    angg.append(aa)
                z=(max(max_v)/len(max_v))
                max_peakavg=(np.average(max_v)-z)
                max_peak=max(max_v)
                imp=max_peak/max_peakavg
                m1,m2=loc[np.argmax(max_v)]
                ang=angg[np.argmax(max_v)]
                print >> sys.stderr, str([z,max_peak,max_peakavg,imp])
                if(imp>0.8):
                    pass
                else:
                    sys.exit(0)                
                img_gray=imt*0
                img_gray[m2,m1]=255
                rows,cols = img_gray.shape
                M = cv2.getRotationMatrix2D((cols/2,rows/2),-(ang),1)#correct the theta offset with detected angle
                img_p1= cv2.warpAffine(img_gray,M,(cols,rows))
                for a in range(0,rows,1):
                    for b in range(0,cols,1):
                        if(img_p1[a,b]>50):
                            print >> sys.stderr, str([a,b])
                            org_x1=b
                            org_y1=a

                img_gray=img_gray*0
                img_gray[(m2),(m1+ w)]=255
                rows,cols = img_gray.shape
                M = cv2.getRotationMatrix2D((cols/2,rows/2),-(ang),1)
                img_p2= cv2.warpAffine(img_gray,M,(cols,rows))
                for a in range(0,rows,1):
                    for b in range(0,cols,1):
                        if(img_p2[a,b]>50):
                            org_x2=b
                            org_y2=a

                a_vec=((org_x2-org_x1),(org_y2-org_y1))
                pon=[]
                #Plot the offset corrected points on the GUI, by correcting the vectors
                for ii in range(0,len(angle),1):
                    theta2,b_mag,ee,xd,yd=angle[ii]
                    r1=(org_y2-org_y1)
                    r2=math.sqrt((math.pow((org_x2-org_x1),2))+(math.pow((org_y2-org_y1),2)))
                    theta1=math.asin(r1/r2)
                    theta1=math.degrees(theta1)
                    xpn=(org_x1+(b_mag*((math.cos(math.radians(theta1+theta2))))))
                    ypn=(org_y1+(b_mag*((math.sin(math.radians(theta1+theta2))))))
                    pon.append([xpn,ypn])
                    if(ee==1):
                        self.ext.append('q')
                    else:
                        self.ext.append('0')
                self.click_pos.Size = (120,80)
                for a in range(0,len(angle),1):
                    x,y=pon[a]
                    newpt = ogl.CircleShape(POINT_RAD)
                    newpt.SetX(x)
                    newpt.SetY(y)
                    self.pix.append([x,y])
                    self.canv.AddShape(newpt)
                    self.point2.append(newpt)
                    newpt.Show(True)
                    #cline.Show(True)
                    self.canv.Redraw(self.dc)
                    self.canv.Update()                
                event.Skip()
            event.Skip()
        else:
            self.plotexc()

    def savept(self,event):
        '''
This method is used to save the user clicked points and the image
'''
        
        if not tflag:
            wcd = 'tactograph template file'
            pwd = os.getcwd()
            open_dlg = wx.FileDialog(self.parent, message = 'Save template',
                                     defaultDir = pwd,
                                     wildcard = wcd,
                                     style = wx.SAVE| wx.CHANGE_DIR)
            if open_dlg.ShowModal() == wx.ID_OK:
                path = open_dlg.GetPath()
                print >> sys.stderr, path

            try:
                b=zip(*self.pix)#sep the x and y value
                xmin=min(b[0][:])
                ymin=min(b[1][:])
                xmax=max(b[0][:])
                ymax=max(b[1][:])
                img = cv2.imread('bin/snap.jpg',0)
                bnd=10
                xmin-=bnd
                ymin-=bnd
                xmax+=bnd
                ymax+=bnd
                crp_img=img[ymin:ymax,xmin:xmax]#crop the image
                cv2.waitKey(0)
                ang_mag=[]
                lib.Image.fromarray(crp_img).convert('L').save(path+'.bmp')
                vec_a=[(xmax-xmin),(ymin-ymin)]#this vector corresponds to template top line reference
                a='template saved'
            except:
                a="template not saved"
            try:
            
                #with respect to reference vector, angle and magnitude of all the points are calculated and stored in a file
                for ii in range(0,len(self.pix)):
                    px,py=self.pix[ii]
                    if(self.ext[ii]=="q"):
                        ee=1
                    else:
                        ee=0
                    nth_vec=[(px-xmin),(py-ymin)] # vector of the Nth point
                    angle=math.degrees(math.acos((np.dot(vec_a,nth_vec))/((np.linalg.norm(vec_a))*(np.linalg.norm(nth_vec)))))#
                    mag=np.linalg.norm(nth_vec)
                    ang_mag.append([angle,mag,ee,px,py])
                np.savetxt(path+'.txt', ang_mag, fmt='%.18e', delimiter=',', newline='\n', header='This is a Theta and R  file,Dont edit this',comments='#')
                b='points saved'
            except:
                b='points not saved'
            wx.MessageBox(a+' and '+b, 'Message',wx.OK | wx.ICON_INFORMATION)
        else:
            self.plotexc()

    def home(self):
        processor.write('h')
        suma=processor.read(1)#Ack
    def showim(im):
        try:
            lib.Image.fromarray(im).show()
        except AttributeError:
            im.show()

if __name__ == '__main__':
    app = wx.App()
    ogl.OGLInitialize()
    processor = processor()
    joinpoints=Joinpoints()
    if processor.open() == None:
        print >> sys.stderr, "Unable to open Tactograph port!"
        sys.exit()
    frame = Tactograph(None,0)
    app.MainLoop()
    app.Destroy()
